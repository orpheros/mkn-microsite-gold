import React, {useState, useEffect} from "react";
import Link from 'next/link';
import Image from 'next/image';
import { useRouter } from 'next/router';
import LoadingPage from "components/loadingPage";
import * as API from "services/api";
import * as errorCode from "services/error_codes";
import { useGoldBuy } from "providers/goldBuyProvider";
import { SET_DATA, SET_PAGE } from "reducers/goldBuyReducer";

export default function BuyInquiry() {
    const router = useRouter();
    const initialLocalState = () => {
        return {
            loadingPage:true,
            loading:false,
            paymentMethods:[],
            account: null,
            nominal:0,
            weight:0,
            selectedPaymentMethod:"",
            isError: false, 
            error_message: ""
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const { globalGoldBuyState, setGlobalGoldBuyState } = useGoldBuy();
    const checkCanTransaction = async() => {
        setLocalState(initialLocalState());

        try {
            var response = await API.Get('/account/gold/can_transaction', 
            `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {
            router.back();
            return true;
        }

        if(response.code == "000") {
            if(response.data.is_eligible != true) {
                router.push("buy/notEligible");
                return true;
            }

            let objLocalState = localState;
            objLocalState.loadingPage = false;
            setLocalState({...objLocalState, ...localState});  
            
            getGoldPrice();
            getPaymentMethod();      
        }
        else router.back();
        return true;
    }
    const getGoldPrice = async() => {
        try {
            var response = await API.Get('/gold/price', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}`);
        }
        catch(error) {     
            router.back();
            return true;
        }

        if(response.code == "000") {
            setGlobalGoldBuyState({
                type: SET_DATA,
                price: parseFloat(response.data.price.buy)
            });
        }
        else router.back();
        return true;
    }
    const getPaymentMethod = async() => {
        let objLocalState = localState;
        objLocalState.paymentMethods = [];
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Get('/payment_method', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}`);
        }
        catch(error) {     
            router.back();
            return true;
        }

        if(response.code == "000") {
            objLocalState.paymentMethods = response.data.paymentMethods;
            setLocalState({...objLocalState, ...localState});            
        }
        else router.back();
        return true;
    }
    const calculateWeight = (nominal) => {        
        let objLocalState = localState;
        objLocalState.nominal = nominal;
        objLocalState.weight = Math.floor(nominal / globalGoldBuyState.price * 1000000) / 1000000;
        setLocalState({...objLocalState, ...localState});
    }
    const proceed = async() => {
        let objLocalState = localState;
        objLocalState.loading = true;
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Post('/account/gold/buy/inquiry', {
                code : localStorage.getItem("merchantcode"),
                client_code : localStorage.getItem("merchantClientcode"),
                token : localStorage.getItem("logined_token"),
                amount : localState.nominal,
                payment_method: localState.selectedPaymentMethod
            });
        }
        catch(error) {     
            router.back();
            return true;
        }

        if(response.code == "000") {
            setGlobalGoldBuyState({
                type: SET_DATA,
                amountGold: parseFloat(response.data.buy.amount_gold),
                amountIDR: parseFloat(response.data.buy.amount_idr),
                price: parseFloat(response.data.buy.price),
                serviceFee: parseFloat(response.data.buy.service_fee),
                paymentMethod: localState.selectedPaymentMethod
            });

            setGlobalGoldBuyState({
                type: SET_PAGE,
                page: "checkout"
            });
        }
        else {
            objLocalState.loading = false;
            objLocalState.isError = true;            
            objLocalState.error_message = response.message ? response.message : errorCode.getErrorCodes(response.code);
            setLocalState({...objLocalState, ...localState});
        }
        return true;
    }
    useEffect(() => {
        checkCanTransaction();        
    }, []);

    return (
        <>
            {globalGoldBuyState.page == "inquiry" &&
                <>
                    {localState.loadingPage
                        ?
                        <LoadingPage />
                        :
                        <div className="sm:container mx-auto p-4 custom-pb-86">
                            <div className="grid grid-cols-3 items-center">
                                <div>
                                    <Link href="/logined/gold">
                                        <a>
                                            <Image src="/img/back.png" 
                                                height="12"
                                                width="6"
                                                alt="back" />
                                        </a>
                                    </Link>
                                </div>

                                <div className="font-epilogue font-bold text-xl text-center">
                                    Buy Emas
                                </div>
                            </div>

                            <div className="mt-10 bg-gray-2 p-3 rounded-lg text-sm">
                                <div>Harga masih bisa berubah. Harga akhir akan mengikuti pada saat Anda melakukan konfirmasi pembelian</div>                                
                            </div>

                            <div className="mt-5 text-center">
                                <div className="text-sm">Harga Beli</div>
                                <div className="font-epilogue font-bold text-xl">IDR {(globalGoldBuyState.price).toLocaleString()}/gr</div>
                            </div>

                            <div className="mt-5">
                                <label className="text-xs">Nilai Pembelian</label>
                                <div className="flex">
                                    <div className="mr-3 text-2xl">IDR</div>
                                    <input type="tel" 
                                        className="border-b w-full text-2xl"
                                        placeholder="Purchase Amount"
                                        min="0"
                                        step="1"
                                        value={localState.nominal}
                                        onInput={(e)=>calculateWeight(e.target.value)} />
                                </div>
                            </div>
                            <div className="text-xs mt-2 color-red">Minimum purchase is Rp. 10.000</div>

                            <div className="mt-5">
                                <label className="text-xs">Metode Pembayaran</label>
                                <select className="border-b w-full"
                                    value={localState.selectedPaymentMethod}
                                    onChange={(e)=>setLocalState({...localState, selectedPaymentMethod:e.target.value})}>
                                    <option value="">Pilih Metode Pembayaran</option>
                                    {localState.paymentMethods.map((paymentMethod, index) => {
                                        return (
                                            <option value={paymentMethod.code}
                                                key={index}>
                                                {paymentMethod.bank}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>

                            <hr className="mt-5" />

                            <div className="mt-5 bg-gray-2 rounded-xl px-5 py-3">
                                <label className="text-xs">Berat</label>
                                <div className="mr-3 text-2xl">{localState.weight} gr</div>
                            </div>

                            {localState.isError && 
                                <div className="mt-5 bg-red-400 px-5 py-3 rounded-lg text-white">
                                    {localState.error_message}
                                </div>
                            }

                            <div className="mt-10">  
                                {localState.loading
                                    ?
                                    <button className="bg-neutral-400 text-white rounded-lg px-5 py-3 font-semibold-500 w-full">
                                        <Image src="/img/loading.png"
                                            alt="loading"
                                            height={16}
                                            width={16}
                                            className="animate-spin" />
                                        <span className="ml-3">Loading ...</span>
                                    </button>
                                    :                
                                    <button className="button-primary rounded-lg px-5 py-3 font-medium w-full block text-center"
                                        onClick={(e)=>proceed()}>
                                        Next
                                    </button>
                                }
                            </div>
                        </div>
                    }
                </>
            }
        </>
    )
}
