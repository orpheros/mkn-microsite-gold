import React, {useState, useEffect} from "react"

export default function TotalGold(props) {
    const initialLocalState = () => {
        return {
            balance:0,
            sellPrice:0
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const setBalance = () => {
        let objLocalState = localState;
        objLocalState.balance = props.gold ?? 0;
        objLocalState.sellPrice = props.sellPrice ?? 0;
        setLocalState({...objLocalState, ...localState});
    }
    useEffect(() => {
        setBalance();
    }, []);

    return (
        <div className="rounded-lg bg-primary text-white p-5 font-medium-500">
            <div className="font-medium">Nilai Portfolio Kamu</div>
            <div className="text-2xl mt-1">
                {(localState.balance).toLocaleString()} Gram
            </div>
            <div className="text-xs">
                Senilai IDR {(localState.sellPrice * localState.balance).toLocaleString()}
            </div>
        </div>
    )
}