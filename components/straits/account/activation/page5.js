import React, {useState, useEffect} from "react";
import Image from 'next/image';

export default function StraitsAccountActivationPage5(props) {
    const initialLocalState = () => {
        return {
            account: {},
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const saveAccount = (e) => {
        e.preventDefault();
        props.saveAccount(localState.account);
        props.changePage(6);
    }
    useEffect(() => {
        let objLocalState = localState;
        objLocalState.account = props.account;
        setLocalState({...objLocalState, ...localState});
    }, []);

    return (
        <>
            <div className="fixed bg-white w-full z-10">
                <div className="sm:container p-4 mx-auto grid grid-cols-3 items-center">
                    <div className="text-left">
                        <button type="button" 
                            onClick={()=>props.changePage(4)}>
                            <Image src="/img/back.webp" 
                                height="24"
                                width="24"
                                alt="back" />
                        </button>
                    </div>
                    <div className="text-center subtitle1">
                        Aktivasi
                    </div>
                    <div className="text-right">5/8</div>
                </div>
            </div>
            <div className="sm:container mx-auto p-4 custom-pt-70">                
                <form onSubmit={(e)=>saveAccount(e)}>
                    <div className="border rounded py-1 px-3">
                        <label>Pekerjaan</label>
                        <select className="w-full bg-transparent"
                            value={localState.account.occupation ?? ""}
                            onChange={(e) => setLocalState({...localState, account:{...localState.account, occupation:e.target.value}})}
                            required={true}>
                            <option value="">Pilih Pekerjaan</option>
                            <option value="housewife">Ibu Rumah Tangga</option>                        
                            <option value="private_employee">Pegawai Swasta</option>
                            <option value="government_employee">Pegawai Negri</option>
                            <option value="student">Pelajar/Mahasiswa</option>
                            <option value="indonesian_migrant_worker">TKI</option>
                            <option value="enterpreneur">Wira Usaha</option>
                            <option value="foundation_board">Yayasan</option>
                            <option value="others">Lainnya</option>
                        </select>
                    </div>

                    <div className="mt-2 border rounded py-1 px-3">
                        <label>Jabatan</label>
                        <select className="w-full bg-transparent"
                            value={localState.account.job_title ?? ""}
                            onChange={(e) => setLocalState({...localState, account:{...localState.account, job_title:e.target.value}})}
                            required={true}>
                            <option value="">Pilih Jabatan</option>
                            <option value="Owner">Pemilik Usaha</option>                        
                            <option value="Director">Direktur</option>
                            <option value="Manager">Manager</option>
                            <option value="Executive">Eksekutif</option>                        
                            <option value="Others">Lainnya</option>
                        </select>
                    </div>

                    <div className="mt-2 border rounded py-1 px-3">
                        <label>Perusahaan</label>
                        <input type="text"
                            placeholder="Perusahaan"
                            className="w-full"
                            value={localState.account.company_name ?? ""}
                            onInput={(e) => setLocalState({...localState, account:{...localState.account, company_name:e.target.value}})}
                            required={true} />
                    </div>

                    <div className="mt-2 border rounded py-1 px-3">
                        <label>Bidang Usaha</label>
                        <select className="w-full bg-transparent"
                            value={localState.account.company_lob ?? ""}
                            onChange={(e) => setLocalState({...localState, account:{...localState.account, company_lob:e.target.value}})}
                            required={true}>
                            <option value="">Pilih Bidang Usaha</option>
                            <option value="Financial">Finansial</option>                        
                            <option value="Technology">Teknologi</option>
                            <option value="Hospitality">Pelayanan Jasa</option>
                            <option value="Logistics">Logistik</option>
                            <option value="Property">Properti</option>
                            <option value="Energy">Energi</option>
                            <option value="Education">Edukasi</option>
                            <option value="Retail">Retail</option>
                            <option value="Consulting">Konsultasi</option>
                            <option value="Others">Lainnya</option>
                        </select>
                    </div>

                    <div className="mt-2 border rounded py-1 px-3">
                        <label>Lama Bekerja</label>
                        <input type="tel"
                            placeholder="Lama Bekerja"
                            className="w-full"
                            value={localState.account.years_of_work ?? ""}
                            onInput={(e) => setLocalState({...localState, account:{...localState.account, years_of_work:e.target.value}})}
                            required={true} />
                    </div>

                    <div className="mt-2 border rounded py-1 px-3">
                        <label>Alamat Perusahaan</label>
                        <textarea placeholder="Alamat Perusahaan"
                            className="w-full"
                            value={localState.account.company_address ?? ""}
                            onInput={(e) => setLocalState({...localState, account:{...localState.account, company_address:e.target.value}})}
                            required={true}>
                        </textarea>
                    </div>

                    <div className="mt-2 border rounded py-1 px-3">
                        <label>Penghasilan Tahunan</label>
                        <input type="tel"
                            placeholder="Penghasilan Tahunan"
                            className="w-full"
                            value={localState.account.income ?? ""}
                            onInput={(e) => setLocalState({...localState, account:{...localState.account, income:e.target.value}})}
                            required={true} />
                    </div>

                    <div className="mt-5 grid grid-cols-2 gap-3">
                        <button className="button-secondary w-full"
                            onClick={()=>props.changePage(5)}
                            type="button">
                            Kembali
                        </button>
                        <button className="button-primary w-full"
                            type="submit">
                            Selanjutnya
                        </button>
                    </div>
                </form>
            </div>
        </>
    )
}
