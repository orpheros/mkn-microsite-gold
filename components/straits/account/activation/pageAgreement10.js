import React, {useState, useEffect} from "react";
import Image from 'next/image';

export default function StraitsAccountActivationPageAgreement10(props) {
    const initialLocalState = () => {
        return {
            account: {},
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const saveAccount = () => {
        let objLocalState = localState;
        objLocalState.account.agreement10 = "true";
        setLocalState({...objLocalState, ...localState});

        props.saveAccount(localState.account);
        props.changePage(8);
    }
    useEffect(() => {
        let objLocalState = localState;
        objLocalState.account = props.account;
        setLocalState({...objLocalState, ...localState});
    }, []);

    return (
        <>
            <div className="fixed bg-white w-full z-10">
                <div className="sm:container p-4 mx-auto grid grid-cols-3 items-center"> 
                    <div>
                        <button onClick={()=>props.changePage(8)}>
                            <Image src="/img/back.png" 
                                height="12"
                                width="6"
                                alt="back" />
                        </button>
                    </div>
                
                    <div className="font-epilogue font-bold text-xl text-center">
                        Ketentuan
                    </div>
                </div>
            </div>

            <div className="sm:container mx-auto p-4 custom-pt-70">
                <div className="mt-10 text-sm">
                    <p className="mb-5">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed varius condimentum purus, rutrum porta libero eleifend id. Quisque aliquam magna libero, vitae tempus sem dapibus quis. Maecenas ultrices ligula posuere sapien pulvinar efficitur. Sed molestie varius nibh sed suscipit. Maecenas laoreet porttitor massa, at viverra sapien sodales consectetur. Phasellus venenatis leo mauris, vel posuere magna congue et. Duis ac nisi varius, mollis nisl sit amet, varius sapien. Nunc condimentum arcu eget enim vehicula, in tristique urna volutpat. Fusce rutrum lacus id sodales pellentesque.</p>
                    <p className="mb-5">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed varius condimentum purus, rutrum porta libero eleifend id. Quisque aliquam magna libero, vitae tempus sem dapibus quis. Maecenas ultrices ligula posuere sapien pulvinar efficitur. Sed molestie varius nibh sed suscipit. Maecenas laoreet porttitor massa, at viverra sapien sodales consectetur. Phasellus venenatis leo mauris, vel posuere magna congue et. Duis ac nisi varius, mollis nisl sit amet, varius sapien. Nunc condimentum arcu eget enim vehicula, in tristique urna volutpat. Fusce rutrum lacus id sodales pellentesque.</p>
                    <p className="mb-5">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed varius condimentum purus, rutrum porta libero eleifend id. Quisque aliquam magna libero, vitae tempus sem dapibus quis. Maecenas ultrices ligula posuere sapien pulvinar efficitur. Sed molestie varius nibh sed suscipit. Maecenas laoreet porttitor massa, at viverra sapien sodales consectetur. Phasellus venenatis leo mauris, vel posuere magna congue et. Duis ac nisi varius, mollis nisl sit amet, varius sapien. Nunc condimentum arcu eget enim vehicula, in tristique urna volutpat. Fusce rutrum lacus id sodales pellentesque.</p>
                    <p className="mb-5">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed varius condimentum purus, rutrum porta libero eleifend id. Quisque aliquam magna libero, vitae tempus sem dapibus quis. Maecenas ultrices ligula posuere sapien pulvinar efficitur. Sed molestie varius nibh sed suscipit. Maecenas laoreet porttitor massa, at viverra sapien sodales consectetur. Phasellus venenatis leo mauris, vel posuere magna congue et. Duis ac nisi varius, mollis nisl sit amet, varius sapien. Nunc condimentum arcu eget enim vehicula, in tristique urna volutpat. Fusce rutrum lacus id sodales pellentesque.</p>
                    <p className="mb-5">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed varius condimentum purus, rutrum porta libero eleifend id. Quisque aliquam magna libero, vitae tempus sem dapibus quis. Maecenas ultrices ligula posuere sapien pulvinar efficitur. Sed molestie varius nibh sed suscipit. Maecenas laoreet porttitor massa, at viverra sapien sodales consectetur. Phasellus venenatis leo mauris, vel posuere magna congue et. Duis ac nisi varius, mollis nisl sit amet, varius sapien. Nunc condimentum arcu eget enim vehicula, in tristique urna volutpat. Fusce rutrum lacus id sodales pellentesque.</p>
                </div>       

                <div className="mt-10 grid grid-cols-2 gap-3">
                    <button className="bg-gray rounded-lg px-5 py-3 font-medium w-full text-white"
                        onClick={()=>props.changePage(8)}>
                        Kembali
                    </button>
                    <button className="button-primary rounded-lg px-5 py-3 font-medium w-full"
                        onClick={()=>saveAccount()}>
                        Selanjutnya
                    </button>
                </div>
            </div>
        </>
    )
}
