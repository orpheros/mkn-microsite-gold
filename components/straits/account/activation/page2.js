import React, {useState, useEffect} from "react";
import Image from 'next/image';
import Resizer from "react-image-file-resizer";
import { toast, ToastContainer } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";

export default function StraitsAccountActivationPage2(props) {
    const initialLocalState = () => {
        return {
            account: {},
            photo:{
                ktp:"/img/image-placeholder.png",
                selfie:"/img/image-placeholder.png",
                npwp:"/img/image-placeholder.png",
            },
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const showImage = (e, type) => {
        let objLocalState = localState;
        if(e.target.files[0]) {
            try {
                Resizer.imageFileResizer(
                    e.target.files[0],
                    1000,
                    1000,
                    "JPEG",
                    100,
                    0,
                    (uri) => {
                        if (type == "ktp") objLocalState.photo.ktp = uri;
                        else if (type == "selfie") objLocalState.photo.selfie = uri;
                        else if (type == "npwp") objLocalState.photo.npwp = uri;
                        setLocalState({...objLocalState, ...localState});
                    },
                    "base64"
                );         
            } 
            catch (err) {
                if (type == "ktp") objLocalState.photo.ktp = "/img/image-placeholder.png";
                else if (type == "selfie") objLocalState.photo.selfie = "/img/image-placeholder.png";
                else if (type == "npwp") objLocalState.photo.npwp = "/img/image-placeholder.png";
                setLocalState({...objLocalState, ...localState});
            }
        }
        else {
            if (type == "ktp") objLocalState.ktp.photo = "/img/image-placeholder.png";
            else if (type == "selfie") objLocalState.ktp.selfie = "/img/image-placeholder.png";
            setLocalState({...objLocalState, ...localState});
        }
    }
    const saveAccount = (e) => {
        e.preventDefault();
        if(localState.photo.ktp == "/img/image-placeholder.png" || 
            localState.photo.selfie == "/img/image-placeholder.png" || 
            localState.photo.npwp == "/img/image-placeholder.png"
        ) {
            toast.error("Pastikan anda mengunggah foto ktp, selfie dan npwp anda");            
        }
        else {
            let objLocalState = localState;
            objLocalState.account.ktp_photo = localState.photo.ktp;
            objLocalState.account.selfie_photo = localState.photo.selfie;
            objLocalState.account.npwp_photo = localState.photo.npwp;
            setLocalState({...objLocalState, ...localState});

            props.saveAccount(localState.account);
            props.changePage(3);
        }
        return true;
    }
    useEffect(() => {            
        let objLocalState = localState;
        objLocalState.account = props.account;
        objLocalState.photo.ktp = (props.account.ktp_photo ?? "/img/image-placeholder.png");
        objLocalState.photo.selfie = (props.account.selfie_photo ?? "/img/image-placeholder.png");
        objLocalState.photo.npwp = (props.account.npwp_photo ?? "/img/image-placeholder.png");
        setLocalState({...objLocalState, ...localState});
    }, []);
    
    return (
        <>
            <div className="fixed bg-white w-full z-10">
                <div className="sm:container p-4 mx-auto grid grid-cols-3 items-center">
                    <div className="text-left">
                        <button type="button" 
                            onClick={()=>props.changePage(1)}>
                            <Image src="/img/back.webp" 
                                height="24"
                                width="24"
                                alt="back" />
                        </button>
                    </div>
                    <div className="text-center subtitle1">
                        Aktivasi
                    </div>
                    <div className="text-right">2/8</div>
                </div>
            </div>
            <div className="sm:container mx-auto p-4 custom-pt-70">
                <form onSubmit={(e)=>saveAccount(e)}>
                    <div className="border rounded py-1 px-3">
                        <label>KTP</label>
                        <input type="tel"
                            placeholder="Nomor KTP"
                            required={true}
                            className="w-full"
                            maxLength={16}
                            value={localState.account.ktp ?? ""}
                            onInput={(e) => setLocalState({...localState, account:{...localState.account, ktp:e.target.value}})} />
                    </div>
                    
                    <div className="mt-2 flex gap-2">       
                        <div className="border rounded grow py-1 px-3">
                            <label>Foto KTP/Paspor</label>
                            <input type="file"
                                accept="image/*;capture=camera"
                                className="w-full"
                                onChange={(e)=>showImage(e, "ktp")} />
                        </div>
                        <div>
                            <Image src={localState.photo.ktp}
                                height="50"
                                width="50"
                                alt="KTP" />
                        </div>
                    </div>

                    <div className="mt-2 flex gap-2">
                        <div className="border rounded grow py-1 px-3">
                            <label>Foto Selfie dengan KTP/Paspor</label>
                            <input type="file"
                                accept="image/*;capture=camera"
                                className="w-full"
                                onChange={(e)=>showImage(e, "selfie")} />
                        </div>
                        <div>
                            <Image src={localState.photo.selfie}
                                height="50"
                                width="50"
                                alt="Selfie" />
                        </div>
                    </div>

                    <div className="border rounded py-1 px-3 mt-2">
                        <label>NPWP</label>
                        <input type="tel"
                            placeholder="Nomor NPWP"
                            maxLength={15}
                            className="w-full"
                            value={localState.account.npwp ?? ""}
                            onInput={(e) => setLocalState({...localState, account:{...localState.account, npwp:e.target.value}})} />
                    </div>

                    <div className="mt-2 flex gap-2">               
                        <div className="border rounded grow py-1 px-3">
                            <label>Foto NPWP</label>
                            <input type="file"
                                accept="image/*;capture=camera"
                                className="w-full"
                                onChange={(e)=>showImage(e, "npwp")} />
                        </div>
                        <div>
                            <Image src={localState.photo.npwp}
                                height="50"
                                width="50"
                                alt="NPWP" />
                        </div>
                    </div>

                    <div className="mt-5 grid grid-cols-2 gap-3">
                        <button className="button-secondary w-full"
                            type="button"
                            onClick={()=>props.changePage(1)}>
                            Kembali
                        </button>
                        <button className="button-primary w-full"
                            type="submit">
                            Selanjutnya
                        </button>
                    </div>
                </form>
                <ToastContainer />
            </div>
        </>
    )
}
