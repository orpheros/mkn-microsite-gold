import React, {useState, useEffect} from "react";
import { useRouter } from 'next/router';
import Image from 'next/image';
import { toast, ToastContainer } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleExclamation } from '@fortawesome/free-solid-svg-icons'
import * as API from "services/api";
import * as errorCode from "services/error_codes";

export default function StraitsAccountActivationPage8(props) {
    const router = useRouter();
    const initialLocalState = () => {
        return {
            account: {},
            loadingButton: false,
            error: {
                is: false,
                message: ""
            }
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const proceed = async() => {
        if(localState.account.agreement1 != "true" &&
            localState.account.agreement2 != "true" &&
            localState.account.agreement3 != "true" &&
            localState.account.agreement4 != "true" &&
            localState.account.agreement5 != "true" &&
            localState.account.agreement6 != "true" &&
            localState.account.agreement7 != "true" &&
            localState.account.agreement8 != "true" &&
            localState.account.agreement9 != "true" &&
            localState.account.agreement10 != "true" &&
            localState.account.agreement11 &&
            localState.account.agreement12 &&
            localState.account.agreement13 &&
            localState.account.agreement14
        ) {
            toast.error("Pastikan anda mengunggah membaca dan menyetujui semua syarat dan ketentuan yang berlaku");  
        }

        let objLocalState = localState;
        objLocalState.loadingButton = true;        
        setLocalState({...objLocalState, ...localState});
        
        try {
            var response = await API.Post('/account/straits/register', {
                code: localStorage.getItem("merchantcode"),
                client_code: localStorage.getItem("merchantClientcode"),
                token: localStorage.getItem("logined_token"),
                pob: localState.account.pob,
                nationality: localState.account.nationality,
                ktp: localState.account.ktp,
                ktp_photo: localState.account.ktp_photo,
                selfie_photo: localState.account.selfie_photo,
                npwp: localState.account.npwp,
                npwp_photo: localState.account.npwp_photo,
                marital_status: localState.account.marital_status,
                spouse: localState.account.spouse,
                ktp_address: localState.account.ktp_address,
                ktp_regency: localState.account.ktp_regency,
                ktp_postalcode: localState.account.ktp_postalcode,
                house_ownership: localState.account.house_ownership,
                domicile_address: localState.account.domicile_address,
                domicile_postalcode: localState.account.domicile_postalcode,
                emergency_contact_name: localState.account.emergency_contact_name,
                emergency_contact_handphone: localState.account.emergency_contact_handphone,
                emergency_contact_relation: localState.account.emergency_contact_relation,
                mother_name: localState.account.mother_name,
                occupation: localState.account.occupation,
                job_title: localState.account.job_title,
                company_name: localState.account.company_name,
                company_lob: localState.account.company_lob,
                years_of_work: localState.account.years_of_work,
                company_address: localState.account.company_address,
                income: localState.account.income,
                experience: localState.account.experience,
                years_experience: localState.account.years_experience,
                field_of_trading: localState.account.field_of_trading,
                source: localState.account.source,
                bank_account: localState.account.bank_account,
                bank_owner: localState.account.bank_owner,
                bank: localState.account.bank
            });
        }
        catch(error) {
            objLocalState.error = {
                is: true,
                message: "Koneksi ke server gagal"
            };
            objLocalState.loadingButton = false;
            setLocalState({...objLocalState, ...localState});
            return true;
        }

        if (response.code == "000") {
            router.push("/logined/index");
            return true;
        }
        else {
            objLocalState.error = {
                is: true,
                message: response.message ? response.message : errorCode.getErrorCodes(response.code)
            };
            objLocalState.loadingButton = false;
            setLocalState({...objLocalState, ...localState});
        }
    }
    useEffect(() => {
        let objLocalState = localState;
        objLocalState.account = props.account;
        setLocalState({...objLocalState, ...localState});
    }, []);

    return (
        <>
            <div className="fixed bg-white w-full z-10">
                <div className="sm:container p-4 mx-auto grid grid-cols-3 items-center">
                    <div className="text-left">
                        <button type="button" 
                            onClick={()=>props.changePage(7)}>
                            <Image src="/img/back.webp" 
                                height="24"
                                width="24"
                                alt="back" />
                        </button>
                    </div>
                    <div className="text-center subtitle1">
                        Aktivasi
                    </div>
                    <div className="text-right">8/8</div>
                </div>
            </div>

            <div className="sm:container mx-auto p-4 custom-pt-70">                
                <div className="grid grid-cols-3 gap-1">
                    <div className="flex item-center col-span-2">
                        <div className="mr-2">
                            <input type="checkbox"
                                disabled={true}
                                checked={(localState.account.agreement1 == "true" ? "checked" : "")} />
                        </div>
                        <div>Saya telah membaca, memahami dan menyetujui Profil Perusahaan Pialang Berjangka</div>                            
                    </div>
                    <div className="text-right">
                        <button className="button-border-gray-2 rounded px-5 py-3"
                            onClick={()=>props.changePage('agreement1')}>
                            <Image src="/img/document-white.png" 
                                height="20"
                                width="19"
                                alt="Baca Ketentuan" />
                        </button>
                    </div>                
                </div>

                <div className="mt-2 grid grid-cols-3 gap-1">
                    <div className="flex item-center col-span-2">
                        <div className="flex item-center">
                            <div className="mr-2">
                                <input type="checkbox"
                                    disabled={true}
                                    checked={(localState.account.agreement2 == "true" ? "checked" : "")} />
                            </div>
                            <div>Saya telah membaca, memahami dan menyetujui Pernyataan Telah Melakukan Simulasi</div>                        
                        </div>                            
                    </div>
                    <div className="text-right">
                        <button className="button-border-gray-2 rounded px-5 py-3"
                            onClick={()=>props.changePage('agreement2')}>
                            <Image src="/img/document-white.png" 
                                height="20"
                                width="19"
                                alt="Baca Ketentuan" />
                        </button>
                    </div>   
                </div>

                <div className="mt-2 grid grid-cols-3 gap-1">
                    <div className="flex item-center col-span-2">
                        <div className="flex item-center">
                            <div className="mr-2">
                                <input type="checkbox"
                                    disabled={true}
                                    checked={(localState.account.agreement3 == "true" ? "checked" : "")} />
                            </div>
                            <div>Saya telah membaca, memahami dan menyetujui Aplikasi Pembukaan Rekening Transaksi</div>
                        </div>                    
                    </div>
                    <div className="text-right">
                        <button className="button-border-gray-2 rounded px-5 py-3"
                            onClick={()=>props.changePage('agreement3')}>
                            <Image src="/img/document-white.png" 
                                height="20"
                                width="19"
                                alt="Baca Ketentuan" />
                        </button>
                    </div>
                </div>

                <div className="mt-2 grid grid-cols-3 gap-1">
                    <div className="flex item-center col-span-2">
                        <div className="flex item-center">
                            <div className="mr-2">
                                <input type="checkbox"
                                    disabled={true}
                                    checked={(localState.account.agreement4 == "true" ? "checked" : "")} />
                            </div>
                            <div>Saya telah membaca, memahami dan menyetujui Dokumen Pemberitahuan Adanya Risiko</div>                        
                        </div>               
                    </div>
                    <div className="text-right">
                        <button className="button-border-gray-2 rounded px-5 py-3"
                            onClick={()=>props.changePage('agreement4')}>
                            <Image src="/img/document-white.png" 
                                height="20"
                                width="19"
                                alt="Baca Ketentuan" />
                        </button>
                    </div>
                </div>

                <div className="mt-2 grid grid-cols-3 gap-1">
                    <div className="flex item-center col-span-2">
                        <div className="mr-2">
                            <input type="checkbox"
                                disabled={true}
                                checked={(localState.account.agreement5 == "true" ? "checked" : "")} />
                        </div>
                        <div>Saya telah membaca, memahami dan menyetujui Perjanjian Pemberian Amanat</div>
                    </div>
                    <div className="text-right">
                        <button className="button-border-gray-2 rounded px-5 py-3"
                            onClick={()=>props.changePage('agreement5')}>
                            <Image src="/img/document-white.png" 
                                height="20"
                                width="19"
                                alt="Baca Ketentuan" />
                        </button>
                    </div>
                </div>

                <div className="mt-2 grid grid-cols-3 gap-1">
                    <div className="flex item-center col-span-2">
                        <div className="mr-2">
                            <input type="checkbox"
                                disabled={true}
                                checked={(localState.account.agreement6 == "true" ? "checked" : "")} />
                        </div>
                        <div>Saya telah membaca, memahami dan menyetujui Pernyataan Bertanggung Jawab atas Kode Akses Transaksi Nasabah</div>
                    </div>
                    <div className="text-right">
                        <button className="button-border-gray-2 rounded px-5 py-3"
                            onClick={()=>props.changePage('agreement6')}>
                            <Image src="/img/document-white.png" 
                                height="20"
                                width="19"
                                alt="Baca Ketentuan" />
                        </button>
                    </div>
                </div>

                <div className="mt-2 grid grid-cols-3 gap-1">
                    <div className="flex item-center col-span-2">
                        <div className="mr-2">
                            <input type="checkbox"
                                disabled={true}
                                checked={(localState.account.agreement7 == "true" ? "checked" : "")} />
                        </div>
                        <div>Saya telah membaca, memahami dan menyetujui Pernyataan bahwa Dana yang Digunakan adalah Milik Nasabah Sendiri</div>                        
                    </div>
                    <div className="text-right">
                        <button className="button-border-gray-2 rounded px-5 py-3"
                            onClick={()=>props.changePage('agreement7')}>
                            <Image src="/img/document-white.png" 
                                height="20"
                                width="19"
                                alt="Baca Ketentuan" />
                        </button>
                    </div>               
                </div>

                <div className="mt-2 grid grid-cols-3 gap-1">
                    <div className="flex item-center col-span-2">
                        <div className="mr-2">
                            <input type="checkbox"
                                disabled={true}
                                checked={(localState.account.agreement8 == "true" ? "checked" : "")} />
                        </div>
                        <div>Saya telah membaca, memahami dan menyetujui Otorisasi Auto Konversi Mata Uang</div>
                    </div>                
                    <div className="text-right">
                        <button className="button-border-gray-2 rounded px-5 py-3"
                            onClick={()=>props.changePage('agreement8')}>
                            <Image src="/img/document-white.png" 
                                height="20"
                                width="19"
                                alt="Baca Ketentuan" />
                        </button>
                    </div>
                </div>

                <div className="mt-2 grid grid-cols-3 gap-1">
                    <div className="flex item-center col-span-2">
                        <div className="mr-2">
                            <input type="checkbox"
                                disabled={true}
                                checked={(localState.account.agreement9 == "true" ? "checked" : "")} />
                        </div>
                        <div>Saya telah membaca, memahami dan menyetujui Peraturan Perdagangan</div>
                    </div>                
                    <div className="text-right">
                        <button className="button-border-gray-2 rounded px-5 py-3"
                            onClick={()=>props.changePage('agreement9')}>
                            <Image src="/img/document-white.png" 
                                height="20"
                                width="19"
                                alt="Baca Ketentuan" />
                        </button>
                    </div>
                </div>

                <div className="mt-2 grid grid-cols-3 gap-1">
                    <div className="flex item-center col-span-2">
                        <div className="mr-2">
                            <input type="checkbox"
                                disabled={true}
                                checked={(localState.account.agreement10 == "true" ? "checked" : "")} />
                        </div>
                        <div>Saya telah membaca, memahami dan menyetujui Persetujuan Penggunaan Data Pribadi</div>
                    </div>                
                    <div className="text-right">
                        <button className="button-border-gray-2 rounded px-5 py-3"
                            onClick={()=>props.changePage('agreement10')}>
                            <Image src="/img/document-white.png" 
                                height="20"
                                width="19"
                                alt="Baca Ketentuan" />
                        </button>
                    </div>
                </div>

                <div className="mt-2">
                    <div className="flex item-center">
                        <div className="mr-2">
                            <input type="checkbox"
                                checked={(localState.account.agreement11 ? "checked" : "")}
                                onChange={(e) => setLocalState({...localState, account:{...localState.account, agreement11:e.target.checked}})} />
                        </div>
                        <div>Seperti halnya transaksi perdagangan pada umumnya, setiap perdagangan komoditas juga memiliki risiko. Mohon Anda mempelajari dan memahami produk yang akan anda transaksikan.</div>
                    </div>                
                </div>

                <div className="mt-2">
                    <div className="flex item-center">
                        <div className="mr-2">
                            <input type="checkbox"
                                checked={(localState.account.agreement12 ? "checked" : "")}
                                onChange={(e) => setLocalState({...localState, account:{...localState.account, agreement12:e.target.checked}})} />
                        </div>                    
                        <div>Saya tidak pernah dinyatakan pailit oleh Pengadilan</div>                        
                    </div>                
                </div>

                <div className="mt-2">
                    <div className="flex item-center">
                        <div className="mr-2">
                            <input type="checkbox" />
                        </div>
                        <div>Saya tidak memiliki anggota keluarga yang bekerja di Bappebti/Bursa Berjangka/Kliring Berjangka</div>                    
                    </div>
                </div>    

                <div className="mt-2">
                    <div className="flex item-center">
                        <div className="mr-2">
                            <input type="checkbox"
                                checked={(localState.account.agreement13 ? "checked" : "")}
                                onChange={(e) => setLocalState({...localState, account:{...localState.account, agreement13:e.target.checked}})} />
                        </div>
                        <div>Saya telah membaca, memahami dan menyetujui Syarat dan Ketentuan PT Straits Futures Indonesia</div>                    
                    </div>
                </div>    

                <div className="mt-2">
                    <div className="flex item-center">
                        <div className="mr-2">
                            <input type="checkbox"
                                checked={(localState.account.agreement14 ? "checked" : "")}
                                onChange={(e) => setLocalState({...localState, account:{...localState.account, agreement14:e.target.checked}})} />
                        </div>
                        <div>Saya mengonfirmasi bahwa seluruh data dan informasi yang saya berikan adalah benar</div>                    
                    </div>
                </div>  

                {localState.error.is && 
                    <div className="mt-2 bg-red-7 p-3 rounded text-white">
                        <FontAwesomeIcon icon={faCircleExclamation}
                            className="mr-1" />
                        {localState.error.message}
                    </div>
                }          

                <div className="mt-10 grid grid-cols-2 gap-3">
                    <button className="button-secondary w-full"
                        onClick={()=>props.changePage(7)}
                        type="button">
                        Kembali
                    </button>

                    {localState.loadingButton
                        ?
                        <button className="button-primary w-full"
                            type="button"
                            disabled="disabled">
                            <Image src="/img/loading.png"
                                alt="loading"
                                height={16}
                                width={16}
                                className="animate-spin" />
                            <span className="ml-3">Loading ...</span>
                        </button>
                        :         
                        <button className="button-primary w-full"
                            onClick={()=>proceed()}
                            type="button">
                            Submit
                        </button>
                    }
                </div>

                <ToastContainer />
            </div>
        </>
    )
}
