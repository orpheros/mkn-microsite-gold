import React, {useState, useEffect} from "react";
import Image from 'next/image';

export default function StraitsAccountActivationPage6(props) {
    const initialLocalState = () => {
        return {
            account: {},
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const saveAccount = (e) => {
        e.preventDefault();
        props.saveAccount(localState.account);
        props.changePage(7);
    }
    useEffect(() => {
        let objLocalState = localState;
        objLocalState.account = props.account;
        setLocalState({...objLocalState, ...localState});
    }, []);

    return (
        <>
            <div className="fixed bg-white w-full z-10">
                <div className="sm:container p-4 mx-auto grid grid-cols-3 items-center">
                    <div className="text-left">
                        <button type="button" 
                            onClick={()=>props.changePage(5)}>
                            <Image src="/img/back.webp" 
                                height="24"
                                width="24"
                                alt="back" />
                        </button>
                    </div>
                    <div className="text-center subtitle1">
                        Aktivasi
                    </div>
                    <div className="text-right">6/8</div>
                </div>
            </div>

            <div className="sm:container mx-auto p-4 custom-pt-70">
                <form onSubmit={(e)=>saveAccount(e)}>                
                    <div className="border rounded py-1 px-3">
                        <label>Apakah Anda Memiliki Pengalaman Investasi</label>
                        <select className="w-full bg-transparent"
                            value={localState.account.experience ?? ""}
                            onInput={(e) => setLocalState({...localState, account:{...localState.account, experience:e.target.value}})}
                            required={true}>
                            <option value="">Pilih</option>
                            <option value="Y">Ya</option>
                            <option value="N">Tidak</option>
                        </select>
                    </div>

                    <div className="mt-2 border rounded py-1 px-3">
                        <label>Jenis Investasi</label>
                        <select className="w-full bg-transparent"
                            value={localState.account.field_of_trading ?? ""}
                            onInput={(e) => setLocalState({...localState, account:{...localState.account, field_of_trading:e.target.value}})}
                            required={true}>
                            <option value="">Pilih</option>
                            <option value="Crypto">Crypto</option>
                            <option value="Stock">Saham</option>
                            <option value="Futures">Futures</option>
                            <option value="Others">Lainnya</option>
                        </select>
                    </div>

                    <div className="mt-2 border rounded py-1 px-3">
                        <label>Pengalaman Investasi</label>
                        <input type="tel"
                            placeholder="Lama tahun pengalaman investasi"
                            className="w-full"
                            value={localState.account.years_experience ?? ""}
                            onInput={(e) => setLocalState({...localState, account:{...localState.account, years_experience:e.target.value}})}
                            required={true} />
                    </div>

                    <div className="mt-2 border rounded py-1 px-3">
                        <label>Sumber Dana</label>
                        <select className="w-full bg-transparent"
                            value={localState.account.source ?? ""}
                            onInput={(e) => setLocalState({...localState, account:{...localState.account, source:e.target.value}})}
                            required={true}>
                            <option value="">Pilih</option>
                            <option value="Salary">Gaji</option>
                            <option value="Business Profit">Keuntungan Perusahaan</option>
                            <option value="Others">Lainnya</option>
                        </select>
                    </div>

                    <div className="mt-5 grid grid-cols-2 gap-3">
                        <button className="button-secondary w-full"
                            onClick={()=>props.changePage(5)}
                            type="button">
                            Kembali
                        </button>
                        <button className="button-primary w-full"
                            type="submit">
                            Selanjutnya
                        </button>
                    </div>
                </form>
            </div>
        </>
    )
}
