import React, {useState, useEffect} from "react";
import Link from 'next/link';
import Image from 'next/image';

export default function StraitsAccountActivationPage1(props) {    
    const initialLocalState = () => {
        return {
            account: {},
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const saveAccount = (e) => {
        e.preventDefault();
        props.saveAccount(localState.account);
        props.changePage(2);
    }
    useEffect(() => {
        let objLocalState = localState;
        objLocalState.account = props.account;
        setLocalState({...objLocalState, ...localState});
    }, []);

    return (
        <>
            <div className="fixed bg-white w-full z-10">
                <div className="sm:container p-4 mx-auto grid grid-cols-3 items-center">            
                    <div className="text-left">
                        <Link href="/logined/index">
                            <a>
                                <Image src="/img/back.webp" 
                                    height="24"
                                    width="24"
                                    alt="back" />
                            </a>
                        </Link>                        
                    </div>
                    <div className="text-center subtitle1">
                        Aktivasi
                    </div>
                    <div className="text-right">1/8</div>
                </div>
            </div>

            <div className="sm:container mx-auto p-4 custom-pt-70">
                <form onSubmit={(e)=>saveAccount(e)}>                    
                    <div className="border rounded py-1 px-3">
                        <label>Nama Lengkap</label>
                        <input type="text"
                            placeholder="Nama Lengkap"
                            className="w-full"
                            disabled={true}
                            value={localState.account.fullname ?? ""} />
                    </div>

                    <div className="mt-2 border rounded py-1 px-3">
                        <label>Tempat Lahir</label>
                        <input type="text"
                            placeholder="Tempat Lahir"
                            required={true}
                            className="w-full"
                            value={localState.account.pob ?? ""}
                            onInput={(e) => setLocalState({...localState, account:{...localState.account, pob:e.target.value}})} />
                    </div>

                    <div className="mt-2 border rounded py-1 px-3">
                        <label>Tanggal Lahir</label>
                        <input type="date"
                            className="w-full"
                            value={localState.account.dob ?? ""}
                            disabled={true} />
                    </div>

                    <div className="mt-2 border rounded py-1 px-3">
                        <label>Kewarganegaraan</label>
                        <select className="w-full bg-transparent"
                            value={localState.account.nationality ?? ""}
                            onChange={(e) => setLocalState({...localState, account:{...localState.account, nationality:e.target.value}})}
                            required={true}>
                            <option value="">Pilih Kewarganegaraan</option>
                            <option value="wni">WNI</option>
                            <option value="wna">WNA</option>
                        </select>
                    </div>

                    <div className="mt-2 border rounded py-1 px-3">
                        <label>Jenis Kelamin</label>
                        <select className="w-full bg-transparent"
                            value={localState.account.gender ?? ""}
                            disabled={true}>
                            <option value="">Pilih Jenis Kelamin</option>
                            <option value="male">Pria</option>
                            <option value="female">Wanita</option>
                        </select>
                    </div>

                    <div className="mt-2 border rounded py-1 px-3">
                        <label>Email</label>
                        <input type="email"
                            placeholder="Alamat Email"
                            className="w-full"
                            value={localState.account.email ?? ""}
                            disabled={true} />
                    </div>

                    <div className="mt-5">
                        <button className="button-primary w-full"                        
                            type="submit">
                            Selanjutnya
                        </button>
                    </div>
                </form>
            </div>
        </>        
    )
}
