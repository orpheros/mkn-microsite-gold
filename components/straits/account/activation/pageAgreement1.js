import React, {useState, useEffect} from "react";
import Image from 'next/image';

export default function StraitsAccountActivationPageAgreement1(props) {
    const initialLocalState = () => {
        return {
            account: {},
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const saveAccount = () => {
        let objLocalState = localState;
        objLocalState.account.agreement1 = "true";
        setLocalState({...objLocalState, ...localState});

        props.saveAccount(localState.account);
        props.changePage(8);
    }
    useEffect(() => {
        let objLocalState = localState;
        objLocalState.account = props.account;
        setLocalState({...objLocalState, ...localState});
    }, []);

    return (
        <>
            <div className="fixed bg-white w-full z-10">
                <div className="sm:container p-4 mx-auto grid grid-cols-3 items-center">        
                    <div>
                        <button onClick={()=>props.changePage(8)}>
                            <Image src="/img/back.png" 
                                height="12"
                                width="6"
                                alt="back" />
                        </button>
                    </div>
                
                    <div className="font-epilogue font-bold text-xl text-center">
                        Ketentuan
                    </div>
                </div>
            </div>

            <div className="sm:container mx-auto p-4 custom-pt-70">
                <div className="text-center font-bold">PROFIL PERUSAHAAN PIALANG BERJANGKA</div>
                <div className="mt-5">
                    <p className="font-bold">Data Umum Perusahaan</p>
                    <p>
                        Nama : PT Straits Futures Indonesia<br/>
                        Alamat : Gold Coast Office, Eiffel Tower, Level 1 Unit C, Jl. Pantai Indah Kapuk, Jakarta 14470<br/>
                        No Telepon : +62 21 5010 3599<br/>
                        Email : cs.id@straitsfinancial.com<br/>
                        Website : www.straitsfinancial.id
                    </p>
                    <p className="mt-5 font-bold">Susunan Pengurus Perusahaan</p>
                    <p>
                        Direktur Utama : Roseline Ho Lih Ling<br/>
                        Direktur : Joseph Lin Minjun<br/>
                        Direktur : A. M. Renti Dolores<br/>
                        Direktur Kepatuhan : Sahala Siringoringo<br/>
                        Komisaris Utama : Chong Ming Yong<br/>
                        Komisaris : Quek Chin Ing
                    </p>
                    <p className="mt-5 font-bold">Pemegang Saham</p>
                    <p>
                        Straits Financial Group Pte Ltd<br/>
                        Elih Hadi
                    </p>
                    <p className="mt-5 font-bold">Nomor dan Tanggal Izin Usaha dari Bappebti</p>
                    <p>
                        1. Izin Usaha Untuk Menyelenggarakan Kegiatan sebagai Pialang Berjangka No. 43/BAPPEBTI/SI/09/2015 tanggal 11 September 2015<br/>
                        2. Persetujuan Sebagai Pialang Berjangka Yang Menyalurkan Amanat Nasabah Ke Bursa Berjangka Luar Negeri No.01/BAPPEBTI/KP/05/2017 tanggal 5 Mei 2017<br/>
                    </p>
                    <p className="mt-5 font-bold">Nomor dan Tanggal Keanggotaan Bursa Berjangka dan Lembaga Kliring Berjangka</p>
                    <p>
                    1. SPAB/167/JFX/04/2016 tanggal 11 April 2016<br/>
                    2. 83/AK-KBI/IX/2016 tanggal 5 September 2016
                    </p>
                    <p className="mt-5 font-bold">Kontrak Berjangka yang Ditawarkan</p>
                    <p>Kontrak Berjangka yang diperdagangkan di Bursa dalam negeri dan luar negeri (PALN) yang masuk dalam daftar Bappebti.</p>
                    <p className="mt-5 font-bold">Biaya secara Rinci yang Dibebankan kepada Nasabah</p>
                    <p>Sesuai dengan produk atau kontrak yang diperdagangkan.</p>
                    <p className="mt-5 font-bold">Nomor atau Alamat Email Jika Terjadi Keluhan</p>
                    <p>
                    1. Nomor telepon: +62 21 5050 8877<br/>
                    2. Email: cs.id@straitsfinancial.com
                    </p>
                    <p className="mt-5 font-bold">Sarana Penyelesaian Perselisihan yang Dipergunakan Apabila Terjadi Perselisihan</p>
                    <p>Badan Arbitrase Perdagangan Berjangka Komoditi (BAKTI)</p>
                    <p className="mt-5 font-bold">Nama-nama Wakil Pialang Berjangka yang Bekerja di Perusahaan Pialang Berjangka</p>
                    <p>
                    1. A. M. Renti Dolores<br/>
                    2. Cindya Alimin<br/>
                    3. Andriana 
                    </p>
                    <p className="mt-5 font-bold">Nama-nama Wakil Pialang Berjangka yang secara Khusus Ditunjuk oleh Pialang Berjangka untuk Melakukan Verifikasi dalam Rangka Penerimaan Nasabah Elektronik Online</p>
                    <p>Andriana</p>
                    <p className="mt-5 font-bold">Nomor Rekening Terpisah (Segregated Account)</p>
                    <p>
                    1. Bank BCA KCU Sudirman: 035-377- 7111 (IDR) dan 035-327-1919 (USD).<br/>
                    2. Bank CIMB Niaga Graha CIMB Niaga: 8001-618-28000 (IDR) dan 8001-618-30940 (USD).
                    </p>
                </div>

                <div className="mt-10 grid grid-cols-2 gap-3">
                    <button className="button-secondary w-full"
                        onClick={()=>props.changePage(8)}>
                        Kembali
                    </button>
                    <button className="button-primary w-full"
                        onClick={()=>saveAccount()}>
                        Selanjutnya
                    </button>
                </div>
            </div>
        </>
    )
}
