import React, {useState, useEffect} from "react";
import Image from 'next/image';
import * as API from "services/api";

export default function StraitsAccountActivationPage3(props) {
    const initialLocalState = () => {
        return {
            account: {},
            regencies: []
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getRegencyCode = async() => {
        try {
            var response = await API.Get('/straits/regency', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}`
            );
        }
        catch(error) {    
            return true;
        }

        if(response.code == "000") {
            let objLocalState = localState;            
            objLocalState.regencies = response.data.regencies;
            setLocalState({...objLocalState, ...localState}); 
        }
    }
    const saveAccount = (e) => {
        e.preventDefault();
        props.saveAccount(localState.account);
        props.changePage(4);
    }
    useEffect(() => {
        let objLocalState = localState;
        objLocalState.account = props.account;
        setLocalState({...objLocalState, ...localState});

        getRegencyCode();
    }, []);

    return (
        <>
            <div className="fixed bg-white w-full z-10">
                <div className="sm:container p-4 mx-auto grid grid-cols-3 items-center">
                    <div className="text-left">
                        <button type="button" 
                            onClick={()=>props.changePage(2)}>
                            <Image src="/img/back.webp" 
                                height="24"
                                width="24"
                                alt="back" />
                        </button>
                    </div>
                    <div className="text-center subtitle1">
                        Aktivasi
                    </div>
                    <div className="text-right">3/8</div>
                </div>
            </div>

            <div className="sm:container mx-auto p-4 custom-pt-70">
                <form onSubmit={(e)=>saveAccount(e)}>
                    <div className="border rounded py-1 px-3">
                        <label>Status Perkawinan</label>
                        <select className="w-full bg-transparent"
                            value={localState.account.marital_status ?? ""}
                            onInput={(e) => setLocalState({...localState, account:{...localState.account, marital_status:e.target.value}})}
                            required={true}>
                            <option value="">Pilih Status Perkawinan</option>
                            <option value="Single">Belum Menikah</option>
                            <option value="Married">Menikah</option>
                            <option value="Widower">Janda/Duda</option>
                        </select>
                    </div>

                    {localState.account.marital_status == "Married" &&
                        <div className="mt-2 border rounded py-1 px-3">
                            <label>Nama Pasangan</label>
                            <input type="text"
                                placeholder="Nama Pasangan"
                                required={true}
                                className="w-full"
                                value={localState.account.spouse ?? ""}
                                onInput={(e) => setLocalState({...localState, account:{...localState.account, spouse:e.target.value}})} />
                        </div>
                    }

                    <div className="border rounded py-1 px-3 mt-2">
                        <label>Alamat Rumah (sesuai KTP/Paspor)</label>
                        <textarea placeholder="Alamat Rumah"
                            className="w-full"
                            required={true}
                            value={localState.account.ktp_address ?? ""}
                            onInput={(e) => setLocalState({...localState, account:{...localState.account, ktp_address:e.target.value}})} />
                    </div>

                    <div className="border rounded py-1 px-3 mt-2">
                        <label>Kode Regensi (sesuai KTP)</label>
                        <select className="w-full bg-transparent"
                            value={localState.account.ktp_regency ?? ""}
                            onInput={(e) => setLocalState({...localState, account:{...localState.account, ktp_regency:e.target.value}})}
                            required={true}>
                            <option value="">Pilih Kode Regensi</option>
                            {localState.regencies.map((item,index) => {
                                return (
                                    <option key={index}
                                        value={item.code}>
                                        {item.regency}
                                    </option>
                                )
                            })}
                        </select>
                    </div>

                    <div className="border rounded py-1 px-3 mt-2">
                        <label>Kode Pos (sesuai KTP)</label>
                        <input type="tel"
                            placeholder="Kode Pos"
                            className="w-full"
                            maxLength={5}
                            value={localState.account.ktp_postalcode ?? ""}
                            onInput={(e) => setLocalState({...localState, account:{...localState.account, ktp_postalcode:e.target.value}})}
                            required={true} />
                    </div>

                    <div className="border rounded py-1 px-3 mt-2">
                        <label>Status Kepemilikan Rumah</label>
                        <select className="w-full bg-transparent"
                            value={localState.account.house_ownership ?? ""}
                            onChange={(e) => setLocalState({...localState, account:{...localState.account, house_ownership:e.target.value}})}
                            required={true}>
                            <option value="">Pilih Status Kepemilikan</option>
                            <option value="Owned">Milik sendiri</option>
                            <option value="Family House">Milik orang tua</option>
                            <option value="Rent">Kontrak</option>
                        </select>
                    </div>

                    <div className="border rounded py-1 px-3 mt-2">
                        <label>Alamat Domisili</label>
                        <textarea placeholder="Alamat Domisili"
                            className="w-full"
                            value={localState.account.domicile_address ?? ""}
                            onChange={(e) => setLocalState({...localState, account:{...localState.account, domicile_address:e.target.value}})}
                            required={true} />
                    </div>

                    <div className="border rounded py-1 px-3 mt-2">
                        <label>Kode Pos Domisili</label>
                        <input type="tel"
                            placeholder="Kode Pos Domisili"
                            className="w-full"
                            maxLength={5}
                            value={localState.account.domicile_postalcode ?? ""}
                            onChange={(e) => setLocalState({...localState, account:{...localState.account, domicile_postalcode:e.target.value}})}
                            required={true} />
                    </div>         

                    <div className="mt-5 grid grid-cols-2 gap-3">
                        <button className="button-secondary w-full"
                            type="button"
                            onClick={()=>props.changePage(2)}>
                            Kembali
                        </button>
                        <button className="button-primary rounded-lg px-5 py-3 font-medium w-full"
                            type="submit">
                            Selanjutnya
                        </button>
                    </div>
                </form>
            </div>
        </>
    )
}
