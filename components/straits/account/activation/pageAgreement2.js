import React, {useState, useEffect} from "react";
import Image from 'next/image';

export default function StraitsAccountActivationPageAgreement2(props) {
    const initialLocalState = () => {
        return {
            account: {},
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const saveAccount = () => {
        let objLocalState = localState;
        objLocalState.account.agreement2 = "true";
        setLocalState({...objLocalState, ...localState});

        props.saveAccount(localState.account);
        props.changePage(8);
    }
    useEffect(() => {
        let objLocalState = localState;
        objLocalState.account = props.account;
        setLocalState({...objLocalState, ...localState});
    }, []);

    return (
        <>
            <div className="fixed bg-white w-full z-10">
                <div className="sm:container p-4 mx-auto grid grid-cols-3 items-center">  
                    <div>
                        <button onClick={()=>props.changePage(8)}>
                            <Image src="/img/back.png" 
                                height="12"
                                width="6"
                                alt="back" />
                        </button>
                    </div>
                
                    <div className="font-epilogue font-bold text-xl text-center">
                        Ketentuan
                    </div>
                </div>
            </div>

            <div className="sm:container mx-auto p-4 custom-pt-70">
                <div className="text-center font-bold">PROFIL PERUSAHAAN PIALANG BERJANGKA</div>
                <div className="mt-5">
                    <p>Yang mengisi formulir di bawah ini:</p>
                    <p className="mt-5">
                        Nama Lengkap : {localState.account.fullname}<br/>
                        Tempat & Tanggal Lahir : {localState.account.pob}, {new Date(localState.account.dob).toLocaleDateString()}<br/>
                        Alamat : {localState.account.ktp_address}<br/>
                        No. KTP/Passport : {localState.account.ktp}
                    </p>
                    <p className="mt-5">Dengan mengisi kolom “Ya” di bawah ini, saya menyatakan bahwa saya telah melakukan simulasi bertransaksi perdagangan berjangka pada PT Straits Futures Indonesia, dan telah memahami tentang tata cara bertransaksi perdagangan berjangka.</p>
                    <p className="mt-5">Demikian Pernyataan ini dibuat dengan sebenarnya dalam keadaan sadar, sehat jasmani dan rohani serta tanpa paksaan apapun dari pihak manapun.</p>
                </div>       

                <div className="mt-10 grid grid-cols-2 gap-3">
                    <button className="button-secondary w-full"
                        onClick={()=>props.changePage(8)}>
                        Kembali
                    </button>
                    <button className="button-primary w-full"
                        onClick={()=>saveAccount()}>
                        Selanjutnya
                    </button>
                </div>
            </div>
        </>
    )
}
