import React from "react";
import Image from 'next/image'

export default function Powered() {
    return (
        <div className="text-center mt-5 mb-5">
            <span className="self-center mr-1">Powered by </span>
            <Image src="/img/straits_logo.png"
                alt="straits"
                height={16}
                width={52}
                className="self-center" />    
        </div>
    )
}