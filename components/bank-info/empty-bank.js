import React, {useState, useEffect} from "react"
import * as API from "services/api"

export default function EmptyBank() {
    const initialLocalState = () => {
        return {
            bank:[]
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getBank = async() => {
        var objLocalState = localState;
        objLocalState.bank = [];
        setLocalState({...objLocalState, ...localState});
        
        try {
            var response = await API.Get('/bank', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}`);
        }
        catch(error) {
            return true;
        }

        if(response.code == "000") {
            objLocalState.bank = response.data.bank;
            setLocalState({...objLocalState, ...localState});
        }
    }
    useEffect(() => {
        getBank();
    }, []);

    return (
        <>
            <div className="mt-7">
                <label className="text-xs">Bank</label>
                <select className="border-b w-full">
                    <option value="">Select Bank</option>
                    {localState.bank.map((item,index) => {
                        return (
                            <option value={item.code}
                                key={index}>
                                {item.name}
                            </option>
                        )
                    })}
                </select>
            </div>

            <div className="mt-5">
                <label className="text-xs">Account Number</label>
                <input type="tel" 
                    className="border-b w-full"
                    placeholder="Bank Account Number" />
            </div>

            <div className="mt-10">
                <button className="button-primary rounded-lg px-5 py-3 font-medium w-full">
                    Save
                </button>
            </div>
        </>
    )
}