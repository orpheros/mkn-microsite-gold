import React, {useState, useEffect} from "react"
import Image from 'next/image'
import Link from 'next/link';
import * as API from "services/api";

export default function HeaderPage(props) {
    const initialLocalState = () => {
        return {
            fullname: "",
            openedSidebar: false,
            notificationCount: 0
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const toggleSidebar = () => {
        let objLocalState = localState;
        objLocalState.openedSidebar = localState.openedSidebar ? false : true;
        setLocalState({...objLocalState, ...localState});
    }
    const getCountNotifications = async() => {
        try {
            var response = await API.Get('/notification/count', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}`
            );
        }
        catch(error) {
            return true;
        }

        if(response.code == "000") {
            let objLocalState = localState;
            objLocalState.notificationCount = response.data.count;
            setLocalState({...objLocalState, ...localState});
        }
        return true;
    }
    useEffect(() => {       
        let objLocalState = localState;
        objLocalState.fullname = localStorage.getItem("logined_user_fullname");
        setLocalState({...objLocalState, ...localState});

        getCountNotifications();
    }, []);

    return (
        <>
            <div className="fixed bg-white w-full z-10">
                <div className="sm:container p-4 mx-auto">       
                    <div className="grid grid-cols-3">
                        <div>
                            <button type="button"
                                onClick={()=>toggleSidebar()}>
                                <Image src="/img/menu.webp"
                                    height="24"
                                    width="24"
                                    alt="Menu" />
                            </button>
                        </div>
                        {props.title == "logo"
                            ?
                            <div className="text-center">
                                <Image src={process.env.NEXT_PUBLIC_LOGO}
                                    height="24"
                                    width="36"
                                    alt="Nyata" />
                            </div>
                            :
                            <div className="text-center subtitle1">
                                {props.title}
                            </div>
                        }
                        
                        <div className="text-right">
                            <Link href="/logined/notification">
                                <a>
                                    {localState.notificationCount > 0 &&
                                        <span className="bg-red-7 rounded-full color-white relative z-10"
                                            style={{
                                                padding:"1px 5px",
                                                top:-18,
                                                left:30,
                                            }}>
                                            {localState.notificationCount}
                                        </span>
                                    }
                                    <Image src="/img/notification.png"
                                        height="24"
                                        width="24"
                                        alt="Notification" />    
                                </a>
                            </Link>                    
                        </div>
                    </div>
                </div>
            </div>

            <div className={"sidebar-container " + (localState.openedSidebar && "open")}>
                <div className="sidebar">
                    <button type="button"
                        onClick={()=>toggleSidebar()}>
                        <Image src="/img/close.webp"
                            height="24"
                            width="24"
                            alt="close" />
                    </button>
                    <div className="mt-5">
                        <div>Selamat Datang, {localState.fullname}</div>
                        <div className="subtitle2"></div>                                                
                    </div>
                    <div className="divide divide-y mt-5">
                        <Link href="/logined/dashboard">
                            <a className="button-link block items-center flex">
                                <Image src="/img/home.webp"
                                    height="16"
                                    width="16"
                                    alt="home" />
                                <span className="ml-2 self-center color-normal">Home</span>
                            </a>
                        </Link>

                        <Link href="/logined/gold">
                            <a className="button-link block items-center flex">
                                <Image src="/img/moneybag.webp"
                                    height="16"
                                    width="16"
                                    alt="emas" />
                                <span className="ml-2 self-center color-normal">Emas</span>
                            </a>
                        </Link>

                        <Link href="/logined/index">
                            <a className="button-link block items-center flex">
                                <Image src="/img/moneybag.webp"
                                    height="16"
                                    width="16"
                                    alt="indeks" />
                                <span className="ml-2 self-center color-normal">Indeks</span>
                            </a>
                        </Link>

                        <Link href="#">
                            <a className="button-link block items-center flex">
                                <Image src="/img/moneybag.webp"
                                    height="16"
                                    width="16"
                                    alt="komoditas" />
                                <span className="ml-2 self-center color-normal">Komoditas</span>
                            </a>
                        </Link>

                        <Link href="/logined/account">
                            <a className="button-link block items-center flex">
                                <Image src="/img/account.webp"
                                    height="16"
                                    width="16"
                                    alt="akun" />
                                <span className="ml-2 self-center color-normal">Akun</span>
                            </a>
                        </Link>
                    </div>
                </div>
            </div>
        </>
    )
}