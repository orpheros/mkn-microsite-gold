import React, {useState, useEffect} from "react";
import Link from 'next/link';
import * as API from "services/api";

export default function News() {
    const initialLocalState = () => {
        return {
            loading: true,
            news: [],
            page: 1
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getNews = async() => {
        setLocalState(initialLocalState);
        let objLocalState = localState;

        try {
            var response = await API.Get('/news', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&page=${localState.page}`);
        }
        catch(error) { 
            console.log(error);
            objLocalState.loading = false;
            setLocalState({...objLocalState, ...localState});
            return true;
        }

        if(response.code == "000") {
            objLocalState.news = response.data.news;  
            objLocalState.loading = false;
            setLocalState({...objLocalState, ...localState});          
        }        
        return true;
    }
    useEffect(() => {
        getNews();
    }, []);

    return (
        <>
            <div className="subtitle1">
                Berita
            </div>
            <div className="divide divide-y">    
                {localState.news.map((item, key) => {
                    return (
                        <div className="flex py-5"
                            key={key}>
                            <Link href={item.link}>
                                <a className="block"
                                    target="_blank" 
                                    rel="noopener noreferrer">
                                    <div className="subtitle2">{item.title}</div>
                                    <div className="mb-1">{new Date(item.source_time).toLocaleString()}</div>
                                    <div>{item.sumary}</div>
                                </a>
                            </Link>                            
                        </div> 
                    )
                })}         
            </div>
        </>
    )
}