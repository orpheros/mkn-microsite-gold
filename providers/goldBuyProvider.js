import React, {useMemo, useReducer, useContext} from 'react';
import reducer, {globalInitialState} from "reducers/goldBuyReducer";

const GoldBuyContext = React.createContext();

function GoldBuyProvider(props) {
    const [globalGoldBuyState, dispatch] = useReducer(reducer, globalInitialState);

    const setGlobalGoldBuyState = (action) => {
        dispatch(action);
    };

    const resetGlobalGoldBuyState = () => {
        dispatch({type:'RESET'})
    };

    const value = useMemo(() => {
        return {globalGoldBuyState, setGlobalGoldBuyState, resetGlobalGoldBuyState};
    }, [globalGoldBuyState]);

    return (
        <GoldBuyContext.Provider value={value}>
            {props.children}
        </GoldBuyContext.Provider>
    );
}

const useGoldBuy = () => useContext(GoldBuyContext);
export { GoldBuyContext, useGoldBuy }
export default GoldBuyProvider;