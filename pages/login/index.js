import React, {useState} from "react"
import { useRouter } from 'next/router'
import Link from 'next/link'
import Image from 'next/image'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleExclamation } from '@fortawesome/free-solid-svg-icons'
import * as API from "services/api"
import * as errorCode from "services/error_codes"

export default function Login() {
    const router = useRouter();
    const initialLocalState=()=>{
        return {
            loading:false, isError:false, error_message:"",
            input_email:"", input_password:""
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const login = async(e) => {
        e.preventDefault();
        let objLocalState = localState;
        objLocalState.loading = true;
        setLocalState({...objLocalState, ...localState});
        
        try {
            var response = await API.Post('/auth/login', {
                code : localStorage.getItem("merchantcode"),
                client_code : localStorage.getItem("merchantClientcode"),
                email : localState.input_email,                
                password : localState.input_password
            });
        }
        catch(error) {                       
            objLocalState.loading = false;
            objLocalState.isError = true;
            objLocalState.error_message = error;
            setLocalState({...objLocalState, ...localState});
            return true;
        }

        if(response.code == "000") {
            objLocalState.loading = false;
            objLocalState.isError = false;
            setLocalState({...objLocalState, ...localState});
            router.push(`/login/otp?token=${response.data.token}`, undefined, { shallow: true });
        }
        else {
            objLocalState.loading = false;
            objLocalState.isError = true;
            objLocalState.error_message = response.message ? response.message : errorCode.getErrorCodes(response.code);
            setLocalState({...objLocalState, ...localState});
        }
    }

    return (
        <>
            <div className="fixed bg-white w-full z-10">
                <div className="sm:container p-4 mx-auto grid grid-cols-3 items-center">
                    <Link href="/">
                        <a>
                            <Image src="/img/back.webp" 
                                height="24"
                                width="24"
                                alt="back" />
                        </a>
                    </Link>
                    <div className="text-center">
                        <Image src={process.env.NEXT_PUBLIC_LOGO}
                            height="24"
                            width="36"
                            alt="Nyata" />
                    </div>
                </div>
            </div>

            <div className="sm:container mx-auto p-4 custom-pt-70">
                <div className="text-center heading5">
                    Login
                </div>
                <div className="mt-3 text-center subtitle1">
                    Masukkan Username dan Password
                </div>
                <form onSubmit={(e)=>login(e)}>
                    <div className="mt-20">
                        <input type="email" 
                            placeholder="Email"
                            className="border rounded p-3 w-full"
                            onInput={(e)=>setLocalState({...localState, input_email:e.target.value})}
                            value={localState.input_email}
                            required={true}
                            autoComplete="false" />
                    </div>
                    <div className="mt-2">
                        <input type="password" 
                            placeholder="Password"
                            className="border rounded p-3 w-full"
                            onInput={(e)=>setLocalState({...localState, input_password:e.target.value})}
                            value={localState.input_password}
                            required={true}
                            autoComplete="false" />
                    </div>
                    {localState.isError && 
                        <div className="mt-2 bg-red-7 p-3 rounded text-white">
                            <FontAwesomeIcon icon={faCircleExclamation}
                                className="mr-1" />
                            {localState.error_message}
                        </div>
                    }
                    <div className="mt-5">
                        {localState.loading
                            ?
                            <button className="button-primary w-full"
                                type="button"
                                disabled="disabled">
                                <Image src="/img/loading.png"
                                    alt="loading"
                                    height={16}
                                    width={16}
                                    className="animate-spin" />
                                <span className="ml-3">Loading ...</span>
                            </button>
                            :      
                            <button className="button-primary w-full"
                                type="submit">
                                Login
                            </button>
                        }
                    </div>
                </form>
                <div className="mt-3 text-center">
                    <Link href="/forgot-password">
                        <a className="button-link w-full block">
                            Lupa Password?
                        </a>
                    </Link>
                </div>
                <div className="mt-10 text-center">
                    <span>Belum punya akun? </span>
                    <Link href="/signup">
                        <a className="link">
                            Buat sekarang
                        </a>
                    </Link>
                </div>
            </div>
        </>
    )
}
