import React, {useState, useEffect} from "react";
import Link from 'next/link';
import Image from 'next/image';
import { useRouter } from "next/router";
import * as API from "services/api";
import LoadingPage from "components/loadingPage";
import HeaderPage from "components/headerPage";
import News from "components/dashboard/news";
import Promo from "components/dashboard/promos";
import FooterPage from "components/footerPage";

export default function Dashboard() {
    const router = useRouter();
    const initialLocalState = () => {
        return {
            loadingPage: true,
            customer: {
                name: "",
                handphone: ""
            },
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getUser = async() => {
        setLocalState(initialLocalState);
        try {
            var response = await API.Get('/customer', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {
            localStorage.removeItem("logined");
            localStorage.removeItem("logined_user_reference");
            localStorage.removeItem("logined_user_fullname");
            localStorage.removeItem("logined_token");

            router.push('/');
            return true;
        }

        if(response.code == "000") {
            let objLocalState = localState;
            objLocalState.customer.fullname = response.data.customer.fullname;
            objLocalState.customer.handphone = response.data.customer.handphone;
            objLocalState.loadingPage = false;
            setLocalState({...objLocalState, ...localState});
        }
        else {
            localStorage.removeItem("logined");
            localStorage.removeItem("logined_user_reference");
            localStorage.removeItem("logined_user_fullname");
            localStorage.removeItem("logined_token");
            router.push('/');
        }
        return true;
    }
    useEffect(() => {
        getUser();
    }, []);

    return (
        <>
            {localState.loadingPage
                ?
                <LoadingPage />
                :
                <>
                    <HeaderPage title="logo" />
                    <div className="sm:container mx-auto p-4 custom-pb-70 custom-pt-70">
                        <div className="grid grid-cols-4 gap-3">
                            <Link href="/logined/gold">
                                <a>
                                    <div className="text-center">
                                        <Image src="/img/gold.png" 
                                            height="48"
                                            width="48" />
                                        <div>Emas</div>
                                    </div>
                                </a>
                            </Link> 
                            <Link href="/logined/index">
                                <a>
                                    <div className="text-center">
                                        <Image src="/img/index.png" 
                                            height="48"
                                            width="48" />
                                        <div>Indeks</div>
                                    </div>
                                </a>
                            </Link>                 
                        </div>
                        <div className="mt-5">
                            <Promo />
                        </div>
                        <div className="mt-5">
                            <News />
                        </div>
                    </div>

                    <FooterPage selected="home" />
                </>
            }
        </>
    )
}
