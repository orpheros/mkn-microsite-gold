import React, {useState, useEffect} from "react";
import Link from 'next/link';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleExclamation } from '@fortawesome/free-solid-svg-icons'
import * as API from "services/api";
import TotalAccount from "components/straits/account/totalAccount";
import LoadingPage from "components/loadingPage";
import HeaderPage from "components/headerPage";
import Powered from "components/straits/powered";
import FooterPage from "components/footerPage";

export default function Index() {
    const router = useRouter();
    const initialLocalState = () => {
        return {
            loadingPage: true, 
            loadingPortfolio: true,
            account: {},
            handphone:"",
            watchlists: [],
            portfolios: []
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getAccount = async() => {
        let objLocalState = localState;
        objLocalState.loadingPage = true;
        objLocalState.account = {};
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Get('/account/straits', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {
            router.push('/logined/dashboard');
            return true;
        }

        if(response.code == "000") {
            objLocalState.account = response.data.straits_account;
            objLocalState.loadingPage = false;
            objLocalState.handphone = response.data.handphone;
            setLocalState({...objLocalState, ...localState});
        }
        else router.push('/logined/dashboard');
        return true;
    }
    const getWatchlist = async() => {
        let objLocalState = localState;
        objLocalState.watchlist = [];
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Get('/account/straits/watchlists', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {
            router.push('/logined/dashboard');
            return true;
        }

        if(response.code == "000") {
            objLocalState.watchlists = response.data.watchlists;
            setLocalState({...objLocalState, ...localState});
        }
    }
    const getPortfolio = async(range = "1") => {
        let objLocalState = localState;
        objLocalState.loadingPortfolio = true;
        objLocalState.portfolios = [];
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Get('/account/straits/transaction/portfolio', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {
            router.push('/logined/dashboard');
            return true;
        }

        if(response.code == "000") {
            objLocalState.portfolios = response.data.transactions;
            objLocalState.loadingPortfolio = false;
            setLocalState({...objLocalState, ...localState});
        }
        return true;
    }
    useEffect(() => {
        getAccount();
        getWatchlist();
        getPortfolio();
    }, []);

    return (
        <>
            {localState.loadingPage
                ?
                <LoadingPage />
                :
                <>
                    <HeaderPage title="Indeks" />
                    <div className="custom-pt-70 custom-pb-70">
                        <div className="sm:container mx-auto p-4">
                            {localState.handphone == "" &&
                                <div className="bg-red-7 p-3 text-white rounded mb-2">
                                    <FontAwesomeIcon icon={faCircleExclamation}
                                        className="mr-1 inline"
                                        style={{height:12}} />
                                    Harap isi nomor handphone anda                           
                                </div>
                            }

                            {Object.keys(localState.account).length === 0 ? 
                                <div className="bg-red-7 p-3 text-white rounded">
                                    <FontAwesomeIcon icon={faCircleExclamation}
                                        className="mr-1 inline"
                                        style={{height:12}} />
                                    Harap aktivasi akun anda untuk menggunakan fitur ini                             
                                </div>
                            : localState.account.isactive == "0" ?
                                <div className="bg-gray-10-75 p-3 text-white rounded">
                                    <FontAwesomeIcon icon={faCircleExclamation}
                                        className="mr-1 inline"
                                        style={{height:12}} />
                                    Harap tunggu, akun anda sedang di verifikasi
                                </div>
                            : localState.account.isactive == "-1" ?
                                <div className="bg-gray-10-75 p-3 text-white rounded">
                                    <FontAwesomeIcon icon={faCircleExclamation}
                                        className="mr-1 inline"
                                        style={{height:12}} />
                                    Harap validasi bank akun anda
                                </div>
                            : localState.account.isactive == "2" ?
                            <div className="bg-gray-10-75 p-3 text-white rounded">
                                <FontAwesomeIcon icon={faCircleExclamation}
                                    className="mr-1 inline"
                                    style={{height:12}} />
                                Bank kamu sedang di verifikasi oleh tim terkait
                            </div>
                            :
                                <TotalAccount balance={localState.account.balance} />
                            }
                                                    
                            <div className="grid grid-cols-4 gap-3 mt-3">
                                {Object.keys(localState.account).length === 0
                                    ?
                                    <Link href="index/account/activation">
                                        <a className="text-center">
                                            <Image src="/img/ico-document.png"
                                                alt="Aktivasi akun"
                                                height="50"
                                                width="50" />
                                            <div className="text-center">Aktivasi</div>
                                        </a>
                                    </Link>
                                    :
                                    <>
                                        {localState.account.isactive == "-1" && 
                                            <Link href="index/account/bank-activation">
                                                <a className="text-center">
                                                    <Image src="/img/ico-document.png"
                                                        alt="Aktivasi akun"
                                                        height="50"
                                                        width="50" />
                                                    <div className="text-center">Aktivasi Bank</div>
                                                </a>
                                            </Link>
                                        }

                                        <Link href="straits/topup">
                                            <a className="text-center">
                                                <Image src="/img/ico-document.png"
                                                    alt="Topup"
                                                    height="50"
                                                    width="50" />
                                                <div className="text-center">Topup</div>
                                            </a>
                                        </Link>

                                        {localState.account.isactive == "1" &&                                         
                                            <Link href="straits/withdraw">
                                                <a className="text-center">
                                                    <Image src="/img/ico-document.png"
                                                        alt="Topup"
                                                        height="50"
                                                        width="50" />
                                                    <div className="text-center">Withdraw</div>
                                                </a>
                                            </Link>
                                        }
                                        <Link href="index/transaction">
                                            <a className="text-center">
                                                <Image src="/img/ico-document.png"
                                                    alt="Transaksi"
                                                    height="50"
                                                    width="50" />
                                                <div className="text-center">Transaksi</div>
                                            </a>
                                        </Link>
                                        <Link href="index/portfolio">
                                            <a className="text-center">
                                                <Image src="/img/ico-document.png"
                                                    alt="Portofolio"
                                                    height="50"
                                                    width="50" />
                                                <div className="text-center">Portofolio</div>
                                            </a>
                                        </Link>
                                    </>
                                }
                            </div>  
                        </div>
                        
                        {localState.account.isactive != "0" &&                    
                            <div className="bg-gray-2 sm:container mx-auto p-4">
                                <div className="grid grid-cols-2">
                                    <div className="subtitle1">
                                        Portofolio
                                    </div>
                                    {localState.portfolios.length > 0 && 
                                        <div className="text-right self-center text-blue-600">
                                            <Link href="/logined/index/portfolio">
                                                <a>
                                                    Lihat
                                                </a>
                                            </Link>
                                        </div>
                                    }
                                </div>
                                
                                {localState.portfolios.length > 0
                                    ? 
                                    <div className="mt-3 grid grid-cols-3 gap-3">
                                        {localState.portfolios.map((item,index) => {
                                            return (
                                                <Link href={{
                                                        pathname: '/logined/index/symbol/[symbol]',
                                                        query: { symbol: item.index.symbol },
                                                    }}
                                                    key={index}>
                                                    <a className="bg-white rounded p-3 block drop-shadow">
                                                        <div className="mb-2 flex">
                                                            <div className="mr-2">
                                                                <Image src={item.index.image}
                                                                    height="20"
                                                                    width="20"
                                                                    className="rounded-full"
                                                                    alt={item.index.symbol}
                                                                    title={item.index.symbol} />
                                                            </div>
                                                            <div>
                                                                <div className="font-semibold">{item.index.symbol}</div>
                                                                <div>{item.change_percent}%</div>
                                                            </div>                                
                                                        </div>
                                                        <div className={"text-white text-center rounded-full mb-3 py-1 " + (item.change <= "0" ? "bg-red-7" : "bg-green-7")}>
                                                            {(item.change * item.volume).toLocaleString()}
                                                        </div>
                                                        <div className="grid grid-cols-2">
                                                            <div>Unit</div>
                                                            <div className="text-right">{item.volume}</div>
                                                        </div>
                                                        <div className="grid grid-cols-2">
                                                            <div>Beli</div>
                                                            <div className="text-right">{item.open_price}</div>
                                                        </div>
                                                        <div className="grid grid-cols-2">
                                                            <div>Last</div>
                                                            <div className="text-right">{item.index.price}</div>
                                                        </div>
                                                    </a>
                                                </Link>
                                            )
                                        })}                        
                                    </div>   
                                    :
                                    <div className="mt-3">
                                        Mulai Siapkan Portofolio Index Kamu
                                    </div>
                                }
                            </div>
                        }

                        <div className="sm:container mx-auto p-4 divide-y divide-slate-200">
                            {localState.watchlists.map((item, index) => {
                                return (
                                    <Link href={{
                                            pathname: '/logined/index/symbol/[symbol]',
                                            query: { symbol: item.symbol },
                                        }}
                                        key={index}>
                                        <a className="block">
                                            <div className="grid grid-cols-2 gap-3 py-3">
                                                <div className="flex">
                                                    <div className="mr-2 self-center">
                                                        <Image src={item.image} 
                                                            height="24"
                                                            width="24"
                                                            alt={item.symbol}
                                                            className="rounded-full" />
                                                    </div>
                                                    <div>
                                                        <div className="font-semibold">{item.symbol}</div>
                                                        <div>{item.name}</div>
                                                    </div>
                                                </div>
                                                <div className="text-right">
                                                    <div className="font-semibold">
                                                        <span className="mr-1 color-gray">{item.currency}</span>
                                                        <span className={(item.change >= 0 ? "color-green" : "color-red")}>{item.price}</span>
                                                    </div>
                                                    <div className={(item.change >= 0 ? "color-green" : "color-red")}>
                                                        <span className="mr-1">{item.change}</span> 
                                                        <span>({item.change_percent}%)</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </Link>
                                )
                            })}
                        </div>

                        <Powered />
                    </div>

                    <FooterPage selected="home" />
                </>
            }
        </>
    )
}
