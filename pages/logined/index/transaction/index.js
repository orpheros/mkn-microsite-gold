import React, {useState, useEffect} from "react";
import Image from 'next/image';
import { useRouter } from 'next/router';
import * as API from "services/api";
import LoadingPage from "components/loadingPage";
import Powered from "components/straits/powered";

export default function Index() {
    const router = useRouter();
    const initialLocalState = () => {
        return {
            loadingPage: true,
            mutations: [],
            range: "1"
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getMutation = async() => {
        let objLocalState = localState;
        objLocalState.loadingPage = true;
        objLocalState.mutations = [];
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Get('/account/straits/transaction', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}&range=${localState.range}`);
        }
        catch(error) {
            router.push('/logined/dashboard');
            return true;
        }

        if(response.code == "000") {
            objLocalState.mutations = response.data.transactions;
            objLocalState.loadingPage = false;
            setLocalState({...objLocalState, ...localState});
        }
        else router.push('/logined/dashboard');
        return true;
    }
    const changeRange = (e) => {
        let objLocalState = localState;
        objLocalState.range = e.target.value;
        setLocalState({...objLocalState, ...localState});
        getMutation();
    }
    useEffect(() => {
        getMutation();
    }, []);

    return (
        <>
            {localState.loadingPage
                ?
                <LoadingPage />
                :
                <>
                <div className="sm:container mx-auto p-4">                        
                    <div className="grid grid-cols-3 items-center">  
                        <div className="text-left">
                            <button type="button" 
                                onClick={() => router.back()}>
                                <Image src="/img/back.webp" 
                                    height="24"
                                    width="24"
                                    alt="back" />
                            </button>
                        </div>
                        <div className="text-center subtitle1">
                            Transaksi
                        </div>
                    </div>
                </div>

                <div className="sm:container mx-auto p-4">                    
                    <div>
                        <select className="w-full bg-transparent text-right"
                            value={localState.range}
                            onChange={(e)=>changeRange(e)}>
                            <option value="1">Hari ini</option>
                            <option value="7">Minggu ini</option>
                            <option value="1month">1 Bulan</option>
                            <option value="2months">2 Bulan</option>
                            <option value="6months">6 Bulan</option>
                        </select>
                    </div>  

                    <div className="mt-5 divide-y">
                        {localState.mutations.length > 0 
                            ?
                            <>
                                {localState.mutations.map((item, index) => {
                                    return (
                                        <div className="py-5"
                                            key={index}>
                                            <div className="grid grid-cols-2">
                                                <div className="flex">
                                                    <div>
                                                        <Image src={item.index.image} 
                                                            height="20"
                                                            width="20"
                                                            alt={item.index.name}
                                                            className="rounded-full" />
                                                    </div>
                                                    <div className="ml-2">
                                                        <div className="subtitle2">{item.index.symbol}</div>
                                                        <div className="color-gray">{item.code}</div>
                                                        <div className="color-gray">{new Date(item.created_at).toLocaleString()}</div>
                                                    </div>
                                                </div>
                                                <div className="text-right">
                                                    {item.straits_order_id != "0"
                                                            ? item.closed_at 
                                                                ? <span className="py-1 px-3 rounded-full text-white bg-red-7">SUCCESS</span> 
                                                                : <span className="py-1 px-3 rounded-full text-white bg-green-7">OPEN</span> 
                                                            : <span className="py-1 px-3 rounded-full text-white bg-red-7">CANCELLED</span>
                                                    }
                                                </div>
                                            </div>
        
                                            <div className="grid grid-cols-3 mt-3">
                                                <div>
                                                    <div>Unit</div>
                                                    <div className="subtitle2">{item.volume}</div>
                                                </div>
                                                <div>
                                                    <div>Beli</div>
                                                    <div className="subtitle2">{item.index.currency} {item.open_price}</div>
                                                </div>
                                                <div>
                                                    <div>Close</div>
                                                    <div className="subtitle2">
                                                        {item.close_price ? item.index.currency + " " + item.close_price : "-" }
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                })}
                            </>
                            :
                            <div className="text-center p-4">Tidak ada transaksi</div>
                        }
                    </div>      

                    <Powered />
                </div>
                </>
            }
        </>
    )
}
