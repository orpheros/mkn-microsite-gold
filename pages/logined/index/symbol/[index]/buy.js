import React, {useState, useEffect} from "react";
import BuyInputUnit from "components/straits/index/buy/buyInputUnit";
import BuyConfirmation from "components/straits/index/buy/buyConfirmation";

export default function SymbolDetailBuy() {
    const initialLocalState = () => {
        return {
            unit: 0,
            page: "inputUnit"
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const changePage = (page, unit) => {
        setLocalState({
           ...localState,
            page: page,
            unit: unit
        });
    }
    return (
        <>
            {localState.page === "inputUnit" 
                ?
                <BuyInputUnit changePage={changePage} />
                :
                <BuyConfirmation changePage={changePage}
                    unit={localState.unit} />
            }
        </>
    )
}
