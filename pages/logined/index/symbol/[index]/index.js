import React, {useState, useEffect, useRef} from "react";
import Link from 'next/link';
import Image from 'next/image';
import { useRouter } from 'next/router';
import * as API from "services/api";
import LoadingPage from "components/loadingPage";
import Powered from "components/straits/powered";
import dynamic from 'next/dynamic';

export default function Symbol() {
    const chartContainerRef = useRef();
    const router = useRouter();
    const initialLocalState = () => {
        return {
            loadingSymbol: true,
            loadingChart: true,
            symbol:{},
            portfolios:[],
            activePortfolio:"",
            charts:[]
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getSymbol = async() => {
        let objLocalState = localState;
        objLocalState.loadingSymbol = true;
        objLocalState.account = {};
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Get('/account/straits/symbol', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}&symbol=${router.query.index}`);
        }
        catch(error) {
            router.back();
            return true;
        }

        if(response.code == "000") {
            let date = new Date(response.data.symbol.updated_at);

            objLocalState.loadingSymbol = false;
            objLocalState.symbol = response.data.symbol;
            objLocalState.symbol.updated_at = date.toLocaleString();
            setLocalState({...objLocalState, ...localState});

            getChart();
        }
    }
    const CandleStickChart = dynamic(
        () => import('components/chart'),
        { ssr: false }
    );
    const ContainerChart = () => {
        return (
            <>
                {!localState.loadingChart && <CandleStickChart data={localState.charts} />}
            </>
        );
    };
    const getChart = async() => {
        let objLocalState = localState;
        objLocalState.loadingChart = true;
        objLocalState.charts = [];
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Get('/account/straits/symbol/chart', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}&symbol=${router.query.index}`);
        }
        catch(error) {
            router.back();
            return true;
        }

        if(response.code == "000") {
            objLocalState.loadingChart = false;
            objLocalState.charts = response.data.charts;
            setLocalState({...objLocalState, ...localState});            

            /*
            const chart = createChart(chartContainerRef.current);
            chart.timeScale().fitContent();
            const lineSeries = chart.addLineSeries();
            lineSeries.setData(localState.charts);
            */
        }
    }
    const getHoldings = async() => {
        let objLocalState = localState;
        objLocalState.loadingHolding = true;
        objLocalState.portfolios = [];
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Get('/account/straits/transaction/portfolio', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}&limit=all&symbol=${router.query.index}`);
        }
        catch(error) {
            router.push('/logined/dashboard');
            return true;
        }

        if(response.code == "000") {
            objLocalState.portfolios = response.data.transactions;
            objLocalState.loadingHolding = false;
            setLocalState({...objLocalState, ...localState});
        }
        else router.push('/logined/dashboard');
        return true;
    }
    const toggleActivePortfolio = (value) => {
        let objLocalState = localState;
        objLocalState.activePortfolio = (localState.activePortfolio === value ? "" : value);                
        setLocalState({...objLocalState, ...localState});        
    }
    useEffect(() => {
        if(!router.isReady) return;        
        getSymbol();
        getHoldings();
    }, [router.isReady]);
    
    return (
        <>
            {localState.loadingSymbol
                ?
                <LoadingPage />
                :
                <>
                    <div className="sm:container p-4 fixed bg-white w-full z-10">                       
                        <div className="grid grid-cols-3 items-center">  
                            <div className="text-left">
                                <button type="button" 
                                    onClick={() => router.back()}>
                                    <Image src="/img/back.webp" 
                                        height="24"
                                        width="24"
                                        alt="back" />
                                </button>
                            </div>
                            <div className="text-center subtitle1">
                                {router.query.index}
                            </div>
                        </div>
                    </div>

                    <div className="sm:container mx-auto p-4 custom-pt-70"
                        style={{paddingBottom:75}}>                        
                        <div className="flex">                        
                            <div className="grow">
                                <div className="color-gray">Index</div>
                                <div className="font-semibold">{localState.symbol.name}</div>
                            </div>                        
                            <div className="mr-3">
                                <Image src={localState.symbol.image} 
                                    height="24"
                                    width="24"
                                    alt={localState.symbol.name}
                                    className="rounded-full" />
                            </div>
                        </div>

                        <div className="mt-5">
                            <div>
                                <span className="mr-1">{localState.symbol.currency}</span>
                                <span className="font-semibold">{localState.symbol.price}</span>
                            </div>
                            <div className={"font-semibold  " + (localState.symbol.change >= 0 ? "color-green" : "color-red")}>
                                <span className="mr-1">{localState.symbol.change}</span> 
                                <span>({localState.symbol.change_percent}%)</span>
                                <span className="color-gray font-normal ml-2">{localState.symbol.updated_at}</span>
                            </div>
                        </div>                 

                        <div className="mt-5 mb-10">
                            <div style={{height:300}}>   
                                <ContainerChart />                                 
                            </div>
                        </div>

                        <div>
                            <div className="mb-1 subtitle1">
                                Tentang {localState.symbol.symbol}
                            </div>
                            <div>{localState.symbol.description}</div>
                        </div>

                        {!localState.loadingHoldings && localState.portfolios.length > 0 &&
                            <div className="mt-5 bg-gray-2 rounded drop-shadow">
                                <div className="mb-1 font-epilogue font-medium text-sm text-black p-4">
                                    Holding
                                </div>
                                <div className="divide-y">
                                    {localState.portfolios.map((item,index) => {
                                        return (
                                            <button className={"py-3 px-4 block w-full rounded transition duration-500" + (item.code == localState.activePortfolio ? " bg-green-2" : "")}
                                                key={index}
                                                onClick={()=>toggleActivePortfolio(item.code)}>
                                                <div className="grid grid-cols-2">
                                                    <div className="text-left">
                                                        <div>Beli</div>
                                                        <div>Value P&L</div>
                                                    </div>
                                                    <div className="text-right">
                                                        <div className="font-semibold">{item.index.currency} {item.open_price * item.volume}</div>
                                                        <div className={"font-semibold " + (item.change <= 0 ? "color-red" : "color-green")}>
                                                            <span className="mr-1">{item.change * item.volume}</span>
                                                            <span>({item.change_percent}%)</span>
                                                        </div>
                                                    </div>                                        
                                                </div>                                        
                                            </button>
                                        )
                                    })}                        
                                </div> 
                            </div>
                        }

                        <Powered />
                    </div>
                                            
                    <div className="fixed bottom-0 left-0 right-0 p-3 bg-white">
                        <div className="grid grid-cols-2 gap-3">
                            <div>
                                {localState.activePortfolio != "" &&
                                    <Link href={`/logined/index/transaction/sell?code=${localState.activePortfolio}`}>
                                        <a className="button-primary block bg-red">
                                            Jual
                                        </a>
                                    </Link>
                                }
                            </div>
                            <div>
                                <Link href={localState.symbol.symbol + "/buy"}>
                                    <a className="button-primary block">
                                        Beli
                                    </a>
                                </Link>
                            </div>
                        </div>
                    </div>                    
                </>
            }
        </>
    )
}
