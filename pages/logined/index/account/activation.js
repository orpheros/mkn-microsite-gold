import React, {useState, useEffect} from "react";
import { useRouter } from 'next/router';
import LoadingPage from "components/loadingPage";
import StraitsAccountActivationPage1 from "components/straits/account/activation/page1";
import StraitsAccountActivationPage2 from "components/straits/account/activation/page2";
import StraitsAccountActivationPage3 from "components/straits/account/activation/page3";
import StraitsAccountActivationPage4 from "components/straits/account/activation/page4";
import StraitsAccountActivationPage5 from "components/straits/account/activation/page5";
import StraitsAccountActivationPage6 from "components/straits/account/activation/page6";
import StraitsAccountActivationPage7 from "components/straits/account/activation/page7";
import StraitsAccountActivationPage8 from "components/straits/account/activation/page8";
import * as API from "services/api";
import StraitsAccountActivationPageAgreement1 from "components/straits/account/activation/pageAgreement1";
import StraitsAccountActivationPageAgreement2 from "components/straits/account/activation/pageAgreement2";
import StraitsAccountActivationPageAgreement3 from "components/straits/account/activation/pageAgreement3";
import StraitsAccountActivationPageAgreement4 from "components/straits/account/activation/pageAgreement4";
import StraitsAccountActivationPageAgreement5 from "components/straits/account/activation/pageAgreement5";
import StraitsAccountActivationPageAgreement6 from "components/straits/account/activation/pageAgreement6";
import StraitsAccountActivationPageAgreement7 from "components/straits/account/activation/pageAgreement7";
import StraitsAccountActivationPageAgreement8 from "components/straits/account/activation/pageAgreement8";
import StraitsAccountActivationPageAgreement9 from "components/straits/account/activation/pageAgreement9";
import StraitsAccountActivationPageAgreement10 from "components/straits/account/activation/pageAgreement10";

export default function IndexAccountActivation() {
    const router = useRouter();
    const initialLocalState = () => {
        return {
            loadingPage: true, 
            account: {},
            page : 1
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getUser = async() => {
        let objLocalState = localState;
        objLocalState.account = {};
        objLocalState.loadingPage = true;
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Get('/customer', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {
            router.push('/logined/dashboard');
            return true;
        }

        if(response.code == "000") {
            objLocalState.loadingPage = false;
            objLocalState.account.fullname = response.data.customer.fullname;
            objLocalState.account.email = response.data.customer.email;
            objLocalState.account.dob = response.data.customer.birthdate;
            objLocalState.account.gender = response.data.customer.gender;
            setLocalState({...objLocalState, ...localState});            
        }     
        else router.push('/logined/dashboard');
        return true;
    }
    const changePage = (page) => {
        setLocalState({...localState, page:page})
    }
    const saveAccount = (account) => {
        let objLocalState = localState;
        objLocalState.account = account;
        setLocalState({...objLocalState, ...localState});
    }
    useEffect(() => {
        getUser();
    }, []);

    return (
        <>
            {localState.loadingPage
                ?
                <LoadingPage />
                :
                    <>
                        {localState.page == 1 ? 
                            <StraitsAccountActivationPage1 account={localState.account} 
                                changePage={changePage}
                                saveAccount={saveAccount} />
                            : localState.page == 2 ? 
                                <StraitsAccountActivationPage2 account={localState.account} 
                                    changePage={changePage}
                                    saveAccount={saveAccount} />
                            : localState.page == 3 ? 
                                <StraitsAccountActivationPage3 account={localState.account} 
                                    changePage={changePage}
                                    saveAccount={saveAccount} />
                            : localState.page == 4 ? 
                                <StraitsAccountActivationPage4 account={localState.account} 
                                    changePage={changePage}
                                    saveAccount={saveAccount} />
                            : localState.page == 5 ? 
                                <StraitsAccountActivationPage5 account={localState.account} 
                                    changePage={changePage}
                                    saveAccount={saveAccount} />
                            : localState.page == 6 ? 
                                <StraitsAccountActivationPage6 account={localState.account} 
                                    changePage={changePage}
                                    saveAccount={saveAccount} />
                            : localState.page == 7 ? 
                                <StraitsAccountActivationPage7 account={localState.account} 
                                    changePage={changePage}
                                    saveAccount={saveAccount} />
                            : localState.page == 8 ? 
                                <StraitsAccountActivationPage8 account={localState.account} 
                                    changePage={changePage}
                                    saveAccount={saveAccount} />
                            : localState.page == "agreement1" ? 
                            <StraitsAccountActivationPageAgreement1 account={localState.account} 
                                changePage={changePage}
                                saveAccount={saveAccount} />
                            : localState.page == "agreement2" ? 
                            <StraitsAccountActivationPageAgreement2 account={localState.account} 
                                changePage={changePage}
                                saveAccount={saveAccount} />
                            : localState.page == "agreement3" ? 
                            <StraitsAccountActivationPageAgreement3 account={localState.account} 
                                changePage={changePage}
                                saveAccount={saveAccount} />
                            : localState.page == "agreement4" ? 
                            <StraitsAccountActivationPageAgreement4 account={localState.account} 
                                changePage={changePage}
                                saveAccount={saveAccount} />
                            : localState.page == "agreement5" ? 
                            <StraitsAccountActivationPageAgreement5 account={localState.account} 
                                changePage={changePage}
                                saveAccount={saveAccount} />
                            : localState.page == "agreement6" ? 
                            <StraitsAccountActivationPageAgreement6 account={localState.account} 
                                changePage={changePage}
                                saveAccount={saveAccount} />
                            : localState.page == "agreement7" ? 
                            <StraitsAccountActivationPageAgreement7 account={localState.account} 
                                changePage={changePage}
                                saveAccount={saveAccount} />
                            : localState.page == "agreement8" ? 
                            <StraitsAccountActivationPageAgreement8 account={localState.account} 
                                changePage={changePage}
                                saveAccount={saveAccount} />
                            : localState.page == "agreement9" ? 
                            <StraitsAccountActivationPageAgreement9 account={localState.account} 
                                changePage={changePage}
                                saveAccount={saveAccount} />
                            :
                            <StraitsAccountActivationPageAgreement10 account={localState.account} 
                                changePage={changePage}
                                saveAccount={saveAccount} />
                        }
                    </>
            }
        </>
    )
}
