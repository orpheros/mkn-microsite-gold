import React, {useState, useEffect} from "react";
import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import * as API from "services/api";
import LoadingPage from "components/loadingPage";
import Powered from "components/straits/powered";

export default function Index() {
    const router = useRouter();
    const initialLocalState = () => {
        return {
            loadingPage: true,
            portfolios: [],
            calculation : {
                totalPortfolio : 0,
                totalPnL: 0,
                totalPnLPercent: 0
            }
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getPortfolio = async() => {
        let objLocalState = localState;
        objLocalState.loadingPage = true;
        objLocalState.portfolios = [];
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Get('/account/straits/transaction/portfolio', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}&limit=all&usd_convertion=true`);
        }
        catch(error) {
            router.push('/logined/dashboard');
            return true;
        }

        if(response.code == "000") {
            objLocalState.portfolios = response.data.transactions;
            objLocalState.calculation.totalPortfolio = response.data.calculation.total;
            objLocalState.calculation.totalPnL = response.data.calculation.totalGain;
            objLocalState.calculation.totalPnLPercent = response.data.calculation.totalGainPercent;
            objLocalState.loadingPage = false;
            setLocalState({...objLocalState, ...localState});
        }
        else router.push('/logined/dashboard');
        return true;
    }
    useEffect(() => {
        getPortfolio();
    }, []);

    return (
        <>
            {localState.loadingPage
                ?
                <LoadingPage />
                :
                <>
                    <div className="sm:container p-4 fixed bg-white w-full z-10">                       
                        <div className="grid grid-cols-3 items-center">  
                            <div className="text-left">
                                <button type="button" 
                                    onClick={() => router.back()}>
                                    <Image src="/img/back.webp" 
                                        height="24"
                                        width="24"
                                        alt="back" />
                                </button>
                            </div>
                            <div className="text-center subtitle1">
                                Portofolio
                            </div>
                        </div>
                    </div>

                    <div className="sm:container mx-auto p-4 custom-pt-70">                        
                        <div className="grid grid-cols-2 p-4 rounded bg-gray-2 drop-shadow">
                            <div className="">Total Portofolio</div>
                            <div className="text-right">
                                <span className="mr-1">USD</span>
                                <span className="subtitle2">{localState.calculation.totalPortfolio.toFixed(2)}</span>
                            </div>

                            <div className="mt-2">Total Keuntungan</div>
                            <div className="mt-2 text-right">
                                <span className="mr-1">{localState.calculation.totalPnL}</span>
                                <span className="subtitle2">({localState.calculation.totalPnLPercent}%)</span>
                            </div>
                        </div>
                        
                        {localState.portfolios.length > 0
                            ?
                            <>
                                <div className="mt-5 grid grid-cols-2 font-semibold">
                                    <div>Index</div>
                                    <div className="text-right">Value/P&L</div>
                                </div>                                                    
                                <div className="divide-y">
                                    {localState.portfolios.map((item,index) => {
                                        return (
                                            <Link href={{
                                                pathname: '/logined/index/symbol/[symbol]',
                                                query: { symbol: item.index.symbol },
                                            }}
                                            key={index}>
                                                <a className="py-5 block">
                                                    <div className="grid grid-cols-2">
                                                        <div className="flex">
                                                            <div>
                                                                <Image src={item.index.image} 
                                                                    height="24"
                                                                    width="24"
                                                                    alt={item.index.name}
                                                                    className="rounded-full" />
                                                            </div>
                                                            <div className="ml-2">
                                                                <div className="font-semibold">{item.index.symbol}</div>
                                                                <div>{item.index.name}</div>
                                                            </div>
                                                        </div>
                                                        <div className="text-right">
                                                            <div className="font-semibold">{item.index.currency} {item.open_price * item.volume}</div>
                                                            <div className={"font-semibold " + (item.change <= 0 ? "color-red" : "color-green")}>
                                                                <span className="mr-1">{item.change * item.volume}</span>
                                                                <span>({item.change_percent}%)</span>
                                                            </div>
                                                        </div>                                        
                                                    </div>                                        
                                                </a>
                                            </Link>
                                        )
                                    })}                        
                                </div>  
                            </>
                            :
                            <div className="mt-10 text-center">
                                Kamu belum memiliki portofolio index
                            </div>
                        }
                    </div>

                    <Powered />
                </>
            }
        </>
    )
}
