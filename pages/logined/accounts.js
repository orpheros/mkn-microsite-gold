import React, {useState, useEffect} from "react";
import Link from 'next/link';
import Image from 'next/image';
import { useRouter } from "next/router";
import * as API from "services/api";
import LoadingPage from "components/loadingPage";
import HeaderPage from "components/headerPage";
import News from "components/dashboard/news";
import Promo from "components/dashboard/promos";
import FooterPage from "components/footerPage";

export default function Account() {
    const router = useRouter();
    const initialLocalState = () => {
        return {
            loadingPage: true,
            accounts:[],
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getUser = async() => {
        setLocalState(initialLocalState);
        try {
            var response = await API.Get('/account', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {
            router.push('/logined/dashboard');
            return true;
        }

        if(response.code == "000") {
            let objLocalState = localState;
            objLocalState.accounts = response.data.accounts;
            objLocalState.loadingPage = false;
            setLocalState({...objLocalState, ...localState});
        }
        else router.push('/logined/dashboard');
        return true;
    }
    useEffect(() => {
        getUser();
    }, []);

    return (
        <>
            {localState.loadingPage
                ?
                <LoadingPage />
                :
                <>
                    <HeaderPage title="logo" />
                    <div className="sm:container mx-auto p-4 custom-pb-70 custom-pt-70">
                        {localState.accounts.length > 0 
                            ?
                            <div>
                                {localState.accounts.map((item,index) => {
                                    return (
                                        <div key={index}
                                            className="py-3">
                                            <div className="flex items-center mb-2">
                                                <div className="subtitle1 flex-grow">{item.type}</div>
                                                <Link href={
                                                    item.type == "Emas" ? "/logined/gold" 
                                                    : item.type == "Indeks & Komoditas" ? "/logined/index"
                                                    : "#"
                                                }>
                                                    <a>
                                                        <Image src="/img/next.webp"
                                                            height="12"
                                                            width="12"
                                                            alt="Next" />
                                                    </a>
                                                </Link>
                                            </div>
                                            <div className="bg-white drop-shadow rounded grid grid-cols-2 p-3 items-center">
                                                <div>Saldo</div>
                                                <div className="text-right">
                                                    <span>{item.type == "Indeks & Komoditas" ? "USD" : "IDR"}</span>
                                                    <span className="ml-1 subtitle1">{item.balance}</span>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                })}
                            </div>
                            :
                            <div className="text-center mt-10">
                                Kamu belum mempunyai rekening<br/>
                                Segera mulai investasi pertama kamu
                            </div>
                        }
                    </div>

                    <FooterPage selected="accounts" />
                </>
            }
        </>
    )
}
