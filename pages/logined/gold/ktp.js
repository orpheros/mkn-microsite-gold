import React, {useState, useEffect} from "react";
import Link from 'next/link';
import Image from 'next/image';
import { useRouter } from 'next/router';
import * as API from "services/api";
import LoadingPage from "components/loadingPage";
import EmptyKTPPage from "components/gold/ktp/empty";
import UploadedKTPPage from "components/gold/ktp/uploaded";

export default function GoldKtp() {
    const router = useRouter();
    const initialLocalState = () => {
        return {
            loadingPage: true, loading: false,            
            ktp:{
                ktp:"",
                photo:"/img/image-placeholder.png",
                selfie:"/img/image-placeholder.png",
                isUploaded:false,
                status:""
            },
            error: {
                is:false,
                message:"",
            }
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getUser = async() => {
        setLocalState(initialLocalState());

        try {
            var response = await API.Get('/account/gold/ktp', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {
            router.back();
            return true;
        }

        if(response.code == "000") {
            let objLocalState = localState;
            objLocalState.loadingPage = false;

            if((response.data.ktp.ktp ?? "") != "") {
                objLocalState.ktp.ktp = response.data.ktp.ktp;
                objLocalState.ktp.status = response.data.ktp.status;
                objLocalState.isUploaded = true;
            }
            else objLocalState.isUploaded = false;
            setLocalState({...objLocalState, ...localState});            
        }     
        else router.back();
        return true;
    }
    useEffect(() => {
        getUser();
    }, []);

    return (
        <>
            {localState.loadingPage
                ?
                <LoadingPage />
                :
                <div className="sm:container mx-auto p-4 custom-pb-86">
                    <div className="grid grid-cols-3 items-center">
                        <div>
                            <Link href="/logined/gold">
                                <a>
                                    <Image src="/img/back.png" 
                                        height="12"
                                        width="6"
                                        alt="back" />
                                </a>
                            </Link>
                        </div>

                        <div className="font-epilogue font-bold text-xl text-center">
                            KTP
                        </div>
                    </div>

                    {!localState.isUploaded ? <EmptyKTPPage /> : <UploadedKTPPage ktp={localState.ktp} />}
                </div>
            }
        </>        
    )
}
