import React, {useState, useEffect} from "react"
import Link from 'next/link'
import Image from 'next/image'
import { useRouter } from 'next/router'
import LoadingPage from "components/loadingPage"
import HeaderPage from "components/headerPage";
import FooterPage from "components/footerPage"
import * as API from "services/api"

export default function Account() {
    const router = useRouter();
    const initialLocalState = () => {
        return {
            fullname: "",
            photo:"/img/person.jpg",
            loading: true,
            loadingLogout:false, 
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getUser = async() => {
        let objLocalState = localState;
        objLocalState.loading = true;
        objLocalState.photo = "/img/person.jpg";
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Get('/customer', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {
            router.push('/logined/dashboard');
            return true;
        }

        if(response.code == "000") {
            objLocalState.fullname = response.data.customer.fullname;
            objLocalState.loading = false;
            if(response.data.customer.photo != "") objLocalState.photo = response.data.photo;
            setLocalState({...objLocalState, ...localState});
        }
        else router.push('/logined/dashboard');
        return true;
    }
    const logout = async() => {
        let objLocalState = localState;
        objLocalState.loadingLogout = true;
        setLocalState({...objLocalState, ...localState});

        try {
            await API.Post('/auth/logout', {
                code : localStorage.getItem("merchantcode"),
                client_code : localStorage.getItem("merchantClientcode"),
                token : localStorage.getItem("logined_token")
            });
        }
        catch(error) {                       
            objLocalState.loadingLogout = false;
            setLocalState({...objLocalState, ...localState});
            return true;
        }

        objLocalState.loading = false;
        setLocalState({...objLocalState, ...localState});
        localStorage.removeItem("logined");
        localStorage.removeItem("logined_user_reference");
        localStorage.removeItem("logined_user_fullname");
        localStorage.removeItem("logined_token");
        router.push('/');    
        return true;    
    }
    useEffect(() => {       
        getUser();
    }, []);
    
    return (
        <>
            {localState.loading
                ?
                <LoadingPage />
                :
                <>
                    <HeaderPage title="Akun" />
                    <div className="sm:container mx-auto p-4 custom-pb-70 custom-pt-70">
                        <div className="text-center">
                            <Image src={localState.photo}
                                height="150"
                                width="150"
                                className="rounded-full" />
                        </div>

                        <div className="subtitle1 text-center mt-2">
                            {localState.fullname}
                        </div>

                        <div className="mt-5">
                            <Link href="account/profile">
                                <a className="flex bg-white w-full drop-shadow p-3">
                                    <div className="mr-3 self-center">
                                        <Image src="/img/contact-info.png"
                                            height="16"
                                            width="16"
                                            alt="profile" />
                                    </div>
                                    <div className="self-center">
                                        Profil
                                    </div>
                                    <div className="mr-3 self-center text-right flex-auto">
                                        <Image src="/img/next.webp"
                                            height="16"
                                            width="16"
                                            alt="Next" />
                                    </div>
                                </a>
                            </Link>
                        </div>

                        <div className="mt-2">
                            <Link href="account/bank-info">
                                <a className="flex bg-white w-full drop-shadow p-3">
                                    <div className="mr-3 self-center">
                                        <Image src="/img/bank.png"
                                            height="16"
                                            width="16"
                                            alt="Bank account info" />
                                    </div>
                                    <div className="self-center">
                                        Akun Bank
                                    </div>
                                    <div className="mr-3 self-center text-right flex-auto">
                                        <Image src="/img/next.webp"
                                            height="16"
                                            width="16"
                                            alt="Next" />
                                    </div>
                                </a>
                            </Link>
                        </div>                

                        <div className="mt-2">
                            <Link href="#">
                                <a className="flex bg-white w-full drop-shadow p-3">
                                    <div className="mr-3 self-center">
                                        <Image src="/img/setting.png"
                                            height="16"
                                            width="16"
                                            alt="Reset Password" />
                                    </div>
                                    <div className="self-center">
                                        Reset Password
                                    </div>
                                    <div className="mr-3 self-center text-right flex-auto">
                                        <Image src="/img/next.webp"
                                            height="16"
                                            width="16"
                                            alt="Next" />
                                    </div>
                                </a>
                            </Link>
                        </div>

                        <div className="mt-2">                                        
                            {localState.loadingLogout
                                ?
                                <button className="flex bg-neutral-200 w-full drop-shadow p-3">
                                    <div className="mr-3 self-center">
                                        <Image src="/img/loading.png"
                                            alt="loading"
                                            height={14}
                                            width={14}
                                            className="animate-spin" />
                                    </div>
                                    <div className="self-center">
                                        Loading
                                    </div>
                                    <div className="mr-3 self-center text-right flex-auto">
                                        <Image src="/img/next.webp"
                                            height="16"
                                            width="16"
                                            alt="Next" />
                                    </div>
                                </button>
                                :
                                <button className="flex bg-white w-full drop-shadow p-3"
                                    onClick={()=>logout()}>
                                    <div className="font-medium self-center">
                                        Keluar
                                    </div>
                                    <div className="mr-3 self-center text-right flex-auto">
                                        <Image src="/img/next.webp"
                                            height="16"
                                            width="16"
                                            alt="Next" />
                                    </div>
                                </button>
                            }
                        </div>
                    </div>

                    <FooterPage selected="account" />
                </>
            }
        </>
    )
}
