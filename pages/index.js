import React, {useState, useEffect} from "react"
import Link from 'next/link'
import Image from 'next/image'
import { useRouter } from "next/router";
import * as API from "services/api"
import SplashScreen from "components/splashScreen"
import ErrorScreen from "components/errorScreen"

export default function Index() {
    const router = useRouter();
    const initialLocalState = () => {
        return {
            loading_page: true,
            error_page: false
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const checkLogined = async() => {        
        if (localStorage.getItem("logined") == "true") {
            router.push('logined/dashboard', undefined, { shallow: true });
        }
        else {
            getMerchant();
        }
    }
    const getMerchant = async() => {
        let objLocalState = localState;
        if (localStorage.getItem("merchantSet") != "true") {
            try {
                var response = await API.Get('/merchant', 'code='+process.env.NEXT_PUBLIC_CODE+'&client_code='+process.env.NEXT_PUBLIC_CLIENTKEY);
            }
            catch(error) {
                objLocalState.loading_page = false;
                objLocalState.error_page = true;
                setLocalState({...objLocalState, ...localState});
                return true;
            }

            if(response.code == "000") {
                localStorage.setItem("merchantSet", "true");
                localStorage.setItem("merchantcode", process.env.NEXT_PUBLIC_CODE);
                localStorage.setItem("merchantClientcode", process.env.NEXT_PUBLIC_CLIENTKEY);
                localStorage.setItem("merchantName", response.data.merchant.name);
                localStorage.setItem("merchantLogo", response.data.merchant.logo);

                objLocalState.loading_page = false;
                objLocalState.error_page = false;
                setLocalState({...objLocalState, ...localState});
                return true;
            }
            else {
                objLocalState.loading_page = false;
                objLocalState.error_page = true;
                setLocalState({...objLocalState, ...localState});
                return true;
            }
        }
        else {
            objLocalState.loading_page = false;
            objLocalState.error_page = false;
            setLocalState({...objLocalState, ...localState});
            return true;
        }
    };
    useEffect(() => {
        checkLogined();
    }, []);

    return (
        <>
            {localState.loading_page
                ?
                <SplashScreen />
                :
                <>
                    {localState.error_page
                        ?
                        <ErrorScreen />
                        :
                        <div className="sm:container mx-auto p-4">
                            <div className="text-center mt-20">
                                <Image src="/img/index-center.png"
                                    alt="MKN"
                                    width="300"
                                    height="274" />
                                <div className="heading5">
                                    Selamat Datang di Beragam Pilihan Investasi Alternatif
                                </div>
                                <div className="mt-3 subtitle1">
                                    Investasi Alternatif Berijin Resmi<br/>
                                    Hanya Untukmu
                                </div>
                                <div className="mt-20">
                                    <Link href="signup">
                                        <a className="button-primary block">
                                            Buat Akun
                                        </a>
                                    </Link>
                                </div>
                                <div className="mt-3">
                                    <Link href="login">
                                        <a className="button-primary block">
                                            Login
                                        </a>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    }
                </>                
            }
        </>
    )
}
