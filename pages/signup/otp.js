import React, {useState, useEffect} from "react"
import Link from 'next/link'
import Image from 'next/image'
import { useRouter } from 'next/router'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleExclamation } from '@fortawesome/free-solid-svg-icons'
import * as API from "services/api"
import * as errorCode from "services/error_codes"

export default function SignupOTP() {
    const router = useRouter();
    const initialLocalState=()=>{
        return {
            isError: false, error_message:"",
            loading:false, loading_email_otp:false, 
            input_email_otp:"", 
            error_message:"", isError: false,
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const sendEmailOTP = async() => {
        let objLocalState = localState;
        objLocalState.loading_email_otp = true;
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Post('/auth/signup/email_otp/send', {
                code : localStorage.getItem("merchantcode"),
                client_code : localStorage.getItem("merchantClientcode"),
                token : router.query.token
            });
        }
        catch(error) {
            objLocalState.isError = true;
            objLocalState.error_message = error;
            setLocalState({...objLocalState, ...localState});
            return true;
        }

        if(response.code == "000") {
            objLocalState.isError = false;
            objLocalState.error_message = "";
            objLocalState.loading_email_otp = false;
            setLocalState({...objLocalState, ...localState});
            return true;
        }

        else {
            objLocalState.isError = true;
            objLocalState.error_message = errorCode.getErrorCodes(response.code);
            objLocalState.loading_email_otp = false;
            setLocalState({...objLocalState, ...localState});
            return true;
        }
    }
    const verifyOTP = async(e) => {
        e.preventDefault();

        let objLocalState = localState;
        objLocalState.loading = true;
        setLocalState({...objLocalState, ...localState});
        
        try {
            var response = await API.Post('/auth/signup/verify', {
                code : localStorage.getItem("merchantcode"),
                client_code : localStorage.getItem("merchantClientcode"),
                token : router.query.token,
                email_otp : localState.input_email_otp,
            });
        }
        catch(error) {            
            objLocalState.loading = false;
            objLocalState.isError = false;
            setLocalState({...objLocalState, ...localState});
            return true;
        }

        if(response.code == "000") {
            objLocalState.loading = false;
            objLocalState.isError = false;
            setLocalState({...objLocalState, ...localState});
            router.push('success', undefined, { shallow: true })
        }
        else {
            objLocalState.loading = false;
            objLocalState.isError = true;
            objLocalState.error_message = response.message ? response.message : errorCode.getErrorCodes(response.code);
            setLocalState({...objLocalState, ...localState});
        }
    }

    return (
        <>
            <div className="fixed bg-white w-full z-10">
                <div className="sm:container p-4 mx-auto grid grid-cols-3 items-center">
                    <Link href="/signup">
                        <a>
                            <Image src="/img/back.webp" 
                                height="24"
                                width="24"
                                alt="back" />
                        </a>
                    </Link>
                    <div className="text-center">
                        <Image src={process.env.NEXT_PUBLIC_LOGO}
                            height="24"
                            width="36"
                            alt="Nyata" />
                    </div>
                </div>
            </div>

            <div className="sm:container mx-auto p-4 custom-pt-70">
                <div className="text-center heading5">
                    Verifikasi OTP
                </div>

                <div className="mt-3 text-center subtitle1">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                </div>
                
                <form onSubmit={(e)=>verifyOTP(e)}>
                    <div className="mt-20 flex items-stretch">
                        <div className="flex-auto self-center">
                            <input type="tel" 
                                placeholder="Email OTP"
                                className="border rounded p-3 w-full"
                                autoFocus={true}
                                required={true}
                                value={localState.input_email_otp}
                                onInput={(e) => setLocalState({...localState, input_email_otp:e.target.value})} />
                        </div>
                        <div className="self-center ml-2">
                            {localState.loading_email_otp
                                ?
                                <button className="button-link"
                                    disabled="disabled"
                                    type="button">
                                    <Image src="/img/loading.png"
                                        alt="loading"
                                        height={12}
                                        width={12}
                                        className="animate-spin" />
                                </button>
                                :
                                <button className="button-link"
                                    type="button"
                                    onClick={()=>sendEmailOTP()}>
                                    Kirim OTP
                                </button>
                            }
                        </div>
                    </div>            

                    {localState.isError && 
                        <div className="mt-2 bg-red-7 p-3 rounded text-white">
                            <FontAwesomeIcon icon={faCircleExclamation}
                                className="mr-1" />
                            {localState.error_message}
                        </div>
                    }

                    <div className="mt-5">
                        {localState.loading
                            ?
                            <button className="button-primary w-full"
                                disabled="disabled"
                                type="submit">
                                <Image src="/img/loading.png"
                                    alt="loading"
                                    height={12}
                                    width={12}
                                    className="animate-spin" />
                                <span className="ml-3">Loading ...</span>
                            </button>
                            :                
                            <button className="button-primary w-full"
                                type="submit">
                                Verifikasi OTP
                            </button>                
                        }
                    </div>
                </form>
            </div>
        </>
    )
}
