import React, {useState} from "react"
import { useRouter } from 'next/router'
import Link from 'next/link'
import Image from 'next/image'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleExclamation } from '@fortawesome/free-solid-svg-icons'
import * as API from "services/api"
import * as errorCode from "services/error_codes"

export default function Signup() {
    const router = useRouter();
    const initialLocalState=()=>{
        return {
            loading:false, 
            input_email:"", input_password:"", input_fullname:"", input_birthdate:"", input_gender:"",
            error_message:"", isError: false,
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const signUp = async(e) => {
        e.preventDefault();
        let objLocalState = localState;
        objLocalState.loading = true;
        setLocalState({...objLocalState, ...localState});
        
        try {
            var response = await API.Post('/auth/signup', {
                code : localStorage.getItem("merchantcode"),
                client_code : localStorage.getItem("merchantClientcode"),
                fullname : localState.input_fullname,
                email : localState.input_email,
                password : localState.input_password,
                gender : localState.input_gender,
                birthdate : localState.input_birthdate
            });
        }
        catch(error) {            
            objLocalState.loading = false;
            objLocalState.isError = true;
            objLocalState.error_message = error;
            setLocalState({...objLocalState, ...localState});
            return true;
        }

        if(response.code == "000") {
            objLocalState.loading = false;
            objLocalState.isError = false;
            setLocalState({...objLocalState, ...localState});
            router.push({
                pathname: "signup/otp",
                query: {
                    token: response.data.token
                }
            });
        }
        else {
            objLocalState.loading = false;
            objLocalState.isError = true;
            objLocalState.error_message = response.message ? response.message : errorCode.getErrorCodes(response.code);
            setLocalState({...objLocalState, ...localState});
        }
    }

    return (
        <>
            <div className="fixed bg-white w-full z-10">
                <div className="sm:container p-4 mx-auto grid grid-cols-3 items-center">
                    <Link href="/">
                        <a>
                            <Image src="/img/back.webp" 
                                height="24"
                                width="24"
                                alt="back" />
                        </a>
                    </Link>
                    <div className="text-center">
                        <Image src={process.env.NEXT_PUBLIC_LOGO}
                            height="24"
                            width="36"
                            alt="Nyata" />
                    </div>
                </div>
            </div>

            <div className="sm:container mx-auto p-4 custom-pt-70">
                <div className="text-center heading5">
                    Buat Akun
                </div>
                <div className="mt-3 text-center subtitle1">
                    Lengkapi Informasi Login Kamu
                </div>
                <form onSubmit={(e)=>signUp(e)}>
                    <div className="mt-20">
                        <input type="text" 
                            placeholder="Nama Lengkap"
                            className="border rounded p-3 w-full"
                            autoFocus={true}
                            required={true}
                            value={localState.input_fullname}
                            onInput={(e) => setLocalState({...localState, input_fullname:e.target.value})} />
                    </div>

                    <div className="mt-2">
                        <input type="email" 
                            placeholder="Email"
                            className="border rounded p-3 w-full" 
                            value={localState.input_email}
                            required={true}
                            onInput={(e) => setLocalState({...localState, input_email:e.target.value})} />
                    </div>

                    <div className="mt-2">
                        <input type="password" 
                            placeholder="Password"
                            className="border rounded p-3 w-full" 
                            value={localState.input_password}
                            required={true}
                            onInput={(e) => setLocalState({...localState, input_password:e.target.value})} />
                    </div>

                    <div className="mt-2">
                        <select className="border rounded p-3 w-full"
                            value={localState.input_gender}
                            required={true}
                            onChange={(e) => setLocalState({...localState, input_gender:e.target.value})}>
                            <option value="">Jenis Kelamin</option>
                            <option value="male">Laki-laki</option>
                            <option value="female">Perempuan</option>
                        </select>
                    </div>

                    <div className="mt-2">
                        <input type="date" 
                            placeholder="Birthdate"
                            className="border rounded p-3 w-full" 
                            value={localState.input_birthdate}
                            required={true}
                            onInput={(e) => setLocalState({...localState, input_birthdate:e.target.value})} />
                    </div>

                    {localState.isError && 
                        <div className="mt-2 bg-red-7 p-3 rounded text-white">
                            <FontAwesomeIcon icon={faCircleExclamation}
                                className="mr-1" />
                            {localState.error_message}
                        </div>
                    }

                    <div className="mt-5">
                        {localState.loading
                            ?
                            <button className="button-primary w-full"
                                disabled="disabled">
                                <Image src="/img/loading.png"
                                    alt="loading"
                                    height={12}
                                    width={12}
                                    className="animate-spin" />
                                <span className="ml-2">Loading ...</span>
                            </button>
                            :                
                            <button className="button-primary w-full"                            
                                type="submit">
                                Buat Akun
                            </button>                
                        }
                    </div>
                </form>

                <div className="mt-3 text-center">
                    <Link href="/login">
                        <a className="button-link w-full block">
                            Sudah Punya Akun?
                        </a>
                    </Link>
                </div>
            </div>
        </>
    )
}
